export function publishSelf() {
  if (!Meteor.userId()) {
    throw new Meteor.Error("Not logged in");
  }

  return Meteor.users.find({ _id: Meteor.userId() }, { fields: { isAdmin: 1, masterPrincipalId: 1 } });
}

Meteor.publish("self", publishSelf);
