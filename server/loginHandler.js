Accounts.registerLoginHandler(function loginHandler(loginRequest) {
  if (!loginRequest.guest) {
    return undefined;
  }
  const userId = Meteor.users.insert({ initialConnectionid: this.connection.id, clientIp: this.connection.clientAddress });
  // send loggedin user's user id
  return {
    userId
  };
});
