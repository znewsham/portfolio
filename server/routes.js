import { FlowRouter } from "meteor/kadira:flow-router";
import "/sections/articles/routes.js";

FlowRouter.route("/", {
  name: "index",
  bitbucketUrl() {
    return `${Meteor.bitbucketRoot}/client/ui/views/index`;
  },
  title() {
    return "Zack Newsham / Home";
  },
  action() {
    return Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("/ui/views/index/index.js"),
      import("/ui/layouts/main/main.js")
    ])
    .then(([{ BlazeLayout }]) => {
      return BlazeLayout.render(
        "mainLayout",
        {
          main: "index"
        }
      );
    });
  }
});

FlowRouter.route("/resume", {
  name: "resume",
  bitbucketUrl() {
    return `${Meteor.bitbucketRoot}/client/ui/views/resume`;
  },
  title() {
    return "Zack Newsham / Resume";
  },
  action() {
    return Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("/ui/views/resume/resume.js"),
      import("/ui/layouts/main/main.js")
    ])
    .then(([{ BlazeLayout }]) => {
      return BlazeLayout.render(
        "mainLayout",
        {
          main: "resume"
        }
      );
    });
  }
});
