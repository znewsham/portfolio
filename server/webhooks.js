import page from "page";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
import { FlowRouter } from "meteor/kadira:flow-router";
import { WebApp } from "meteor/webapp";
import "meteor/blaze";
import "./index.html";
import "./routes.js";

const header = Blaze.toHTML(Template.___head___);
const body = Blaze.toHTML(Template.___body___);
const cache = {

};

async function computeAndCache(cacheName, route, params) {
  try {
    const { element, view } = await route.action(params);
    return new Promise((resolve) => {
      let timeout;
      const comp = Tracker.autorun(() => {
        BlazeLayout.readyDep.depend();
        if (view._isReady) {
          comp.stop();
          clearTimeout(timeout);
          resolve();
        }
      });

      setTimeout(() => {
        comp.stop();
        resolve();
      }, 500);
    })
    .then(() => {
      cache[cacheName] = element.innerHTML;
      Blaze._destroyView(view);
      return cache[cacheName];
    });
  }
  catch (e) {
    console.error(e);
    cache[cacheName] = false;
    return Promise.reject(e);
  }
}

function getContext(req) {
  const _page = page.create();
  let currentContext;
  FlowRouter._routes.forEach((route) => {
    _page(route.pathDef, (ctx) => {
      currentContext = {
        route,
        ctx
      };
    });
  });
  _page(req.url);
  return currentContext;
}

// @depracated in favour of direct modifications
function hijackWrite(req, res) {
  const originalWrite = res.write;
  res.write = Meteor.bindEnvironment((chunk, encoding) => {
    // prevent hijacking other http requests
    if (!res.iInjectedHead && encoding === undefined && /^<!DOCTYPE html>/.test(chunk)) {
      chunk = chunk.toString();
      chunk = chunk.replace("<head>", header);
    }
    if (!res.iInjectedBody && encoding === undefined && /<\/body>/.test(chunk)) {
      const cacheName = req.url;
      if (cache[cacheName] === undefined) {
        const currentContext = getContext(req);
        if (currentContext && currentContext.ctx) {
          Meteor.defer(() => {
            computeAndCache(cacheName, currentContext.route, currentContext.ctx.params);
          });
          chunk = chunk.toString();
          const content = cache[cacheName] ? `<div id="__blaze-root">${cache[cacheName]}</div>` : "";
          chunk = chunk.replace("</body>", `${body}${content}</div></body>`);
        }
        else {
          cache[cacheName] = false;
        }
      }
      else {
        chunk = chunk.toString();
        const content = cache[cacheName] ? `<div id="__blaze-root">${cache[cacheName]}</div>` : "";
        chunk = chunk.replace("</body>", `${body}${content}</body>`);
      }
      res.iInjectedBody = true;
    }
    originalWrite.call(res, chunk, encoding);
  });
}

async function populateHtml(html, req) {
  const cacheName = req.url;
  if (cache[cacheName] === undefined) {
    const currentContext = getContext(req);
    if (currentContext && currentContext.ctx) {
      await computeAndCache(cacheName, currentContext.route, currentContext.ctx.params);
      const content = cache[cacheName] ? `<div id="__blaze-root">${cache[cacheName]}</div>` : "";
      html = html.replace("</body>", `${body}${content}</body>`);
    }
    else {
      cache[cacheName] = false;
    }
  }
  else {
    const content = cache[cacheName] ? `<div id="__blaze-root">${cache[cacheName]}</div>` : "";
    html = html.replace("</body>", `${body}${content}</body>`);
  }
  html = html.replace("<head>", `<head>${header}`);
  return html;
}

WebApp.connectHandlers.use((req, res, next) => {
  const bp = WebAppInternals.getBoilerplate({}, "web.browser");
  const html = bp.stream._sources.map(s => s[0].toString()).join("");
  populateHtml(html, req)
  .then((populatedHtml) => {
    res.write(populatedHtml);
  })
  .catch(() => {
    res.write("unexpected error");
  })
  .then(() => {
    res.end();
  });
});

const oldReload = WebAppInternals.reloadClientPrograms;
/*WebAppInternals.reloadClientPrograms = function reloadClientPrograms() {
  oldReload();
  FlowRouter._routes.forEach((route) => {
    if (!route.pathDef.includes(":")) {
      Meteor.defer(() => {
        const bp = WebAppInternals.getBoilerplate({}, "web.browser");
        const html = bp.stream._sources.map(s => s[0].toString()).join("");
        populateHtml(html, { url: route.pathDef })
        .then((populatedHtml) => {
          WebAppInternals.staticFilesByArch["web.browser"][route.pathDef] = { content: populatedHtml };
          WebAppInternals.staticFilesByArch["web.browser.legacy"][route.pathDef] = { content: populatedHtml };
        });
      });
    }
  });
}*/
Meteor.startup(() => {
  // NOTE: pre-cache
});
