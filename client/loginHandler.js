Meteor.loginAsGuest = function (callback) {
  // create a login request with admin: true, so our loginHandler can handle this request
  const loginRequest = { guest: true, connectionId: Meteor.connection._lastSessionId };

  // send the login request
  Accounts.callLoginMethod({
    methodArguments: [loginRequest],
    userCallback: callback
  });
};
