import { FlowRouter } from "meteor/kadira:flow-router";

Meteor.RouteLoading = new ReactiveVar(false);

FlowRouter.triggers.enter((current) => {
  if (current.route.options.routeLoading !== false) {
    if (Meteor.RouteLoading) {
      Meteor.RouteLoading.set(true);
    }
  }
});
FlowRouter.triggers.exit((current) => {
  if (current.route.options.routeLoading !== false) {
    if (Meteor.RouteLoading) {
      Meteor.RouteLoading.set(true);
    }
  }
});

FlowRouter.triggers.exit(() => {
  $(".modal").modal("hide");
});

FlowRouter.notFound = {
  action() {
    Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("/ui/components/notFound/notFound.js"),
      import("/ui/layouts/main/main.js")
    ])
    .then(([{ BlazeLayout }]) => {
      BlazeLayout.render(
        "mainLayout",
        {
          main: "notFound"
        }
      );
    });
  }
};

FlowRouter.route("/", {
  name: "index",
  bitbucketUrl() {
    return `${Meteor.bitbucketRoot}/ui/views/index`;
  },
  title() {
    return "Zack Newsham / Home";
  },
  action() {
    Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("/ui/views/index/index.js"),
      import("/ui/layouts/main/main.js")
    ])
    .then(([{ BlazeLayout }]) => {
      BlazeLayout.render(
        "mainLayout",
        {
          main: "index"
        }
      );
    });
  }
});

FlowRouter.route("/resume", {
  name: "resume",
  bitbucketUrl() {
    return `${Meteor.bitbucketRoot}/ui/views/resume`;
  },
  title() {
    return "Zack Newsham / Resume";
  },
  action() {
    Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("/ui/views/resume/resume.js"),
      import("/ui/layouts/main/main.js")
    ])
    .then(([{ BlazeLayout }]) => {
      BlazeLayout.render(
        "mainLayout",
        {
          main: "resume"
        }
      );
    });
  }
});
