import "./routes.js";
import "/sections/hive/client/index.js";
import "/sections/explore/client/index.js";
import "/sections/articles/client/index.js";
import "/sections/demos/client/index.js";
import "./loginHandler.js";

Meteor.bitbucketRoot = "https://bitbucket.org/znewsham/portfolio/src/master";
