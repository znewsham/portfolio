import SimpleSchema from "simpl-schema";

export default new SimpleSchema({
  name: {
    type: String
  },
  category: {
    type: String
  },
  tags: {
    type: Array
  },
  "tags.$": {
    type: String
  },
  date: {
    type: Date
  }
});
