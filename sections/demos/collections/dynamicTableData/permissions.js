import { DynamicTableData } from "./dynamicTableData.js";

DynamicTableData.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  }
});
