import SimpleSchema from "simpl-schema";

const EncSchema = new SimpleSchema({
  _: {
    type: String,
    optional: true
  }
});
export default new SimpleSchema({
  _enc: {
    type: EncSchema,
    optional: true
  },
  name: {
    type: String
  },
  content: {
    type: SimpleSchema.oneOf(String, Uint8Array)
  },
  date: {
    type: SimpleSchema.oneOf(Date, Uint8Array)
  }
});
