import { EncryptedMessages } from "./encryptedMessages.js";

EncryptedMessages.allow({
  insert() {
    return true;
  }
});
EncryptedMessages.deny({
  insert() {
    return false;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  }
});
