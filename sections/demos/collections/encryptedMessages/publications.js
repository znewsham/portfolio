import { EncryptedMessages } from "./encryptedMessages.js";

Meteor.publish("encryptedMessages", () => {
  return EncryptedMessages.find();
});
