import { ValidatedMethod } from "meteor/mdg:validated-method";
import SimpleSchema from "simpl-schema";

let methodModule;
if (Meteor.isServer) {
  import * as mm from "./server/methods.js";

  methodModule = mm;
}
export const fetchDynamicTableData = new ValidatedMethod({
  name: "demos.fetchDynamicTableData",
  validate: new SimpleSchema({
  }).validator(),
  run() {
    if (Meteor.isServer) {
      return methodModule.fetchDynamicTableData.call(this);
    }
  }
});
