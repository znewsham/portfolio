import moment from "moment";
import _ from "underscore";
import deep from "underscore.deep";
import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { Notifications } from "meteor/gfk:notifications";
import {
  FlexTemplate, FlexTemplates, DynamicTableSpec, DynamicTableSpecs
} from "meteor/portfolio-private";
import "meteor/znewsham:dynamic-table";
import { fetchDynamicTableData } from "../../../method-stubs.js";
import select2Init from "/vendors/select2.full.min.js";
import "./dynamicTables.html";


require("meteor/underscore")._.mixin(deep);
_.mixin(deep);

if (Meteor.isClient) {
  select2Init(window, $);
}

const TempDynamicTableData = new Mongo.Collection(null);


const _tableSpec = {
  collection: TempDynamicTableData,
  bSearching: false,
  searching: false,
  search: false,
  lengthChange: false,
  columns: [
    {
      data: "imageSrc",
      title: "",
      manageFieldsTitle: "Logo",
      render(val) {
        return `<img src="${val}" width="50px" />`;
      }
    },
    {
      data: "title",
      title: "Title",
      editTmpl: Template.dynamicTableSingleValueTextEditor,
      filterModal: true
    },
    {
      data: "tags",
      title: "Tags",
      filterModal: {
        options: ["open-source", "work-experience", "personal-projects", "research"]
      },
      render(val) {
        return (val || []).join(", ");
      },
      editTmpl: Template.dynamicTableSelect2ValueEditor,
      editTmplContext(context) {
        return _.extend(
          context,
          {
            multiple: true,
            tags: true,
            options(doc) {
              return _.union(doc.tags || [], ["open-source", "work-experience", "personal-projects", "research"])
              .map(tag => ({
                id: tag,
                text: tag
              }));
            }
          }
        );
      }
    },
    {
      data: "slug",
      title: "Slug",
      editTmpl: Template.dynamicTableSingleValueTextEditor
    },
    {
      data: "description",
      title: "Description",
      editTmpl: Template.dynamicTableSingleValueTextEditor
    },
    {
      data: "lastEdited",
      title: "Last Edited",
      render(val) {
        return moment(val).format("YYYY-MM-DD");
      }
    }
  ]
};

export class DynamicTablesComponent extends BlazeComponent {
  static HelperMap() {
    return [
      "availableColumns",
      "groupableFields",
      "savedTableSpecFn",
      "manageFieldsOptions",
      "tableSpec",
      "isReady"
    ];
  }

  init() {
    const existingData = Meteor.isClient && Session.get("dynamicTableData");
    this._isReady = new ReactiveVar(false);


    const dynamicTableSpecs = Meteor.isClient && Session.get("dynamicTableSpecs");
    if (dynamicTableSpecs) {
      dynamicTableSpecs.forEach((row) => {
        DynamicTableSpecs.insert(row);
      });
    }

    const flexTemplates = Meteor.isClient && Session.get("flexTemplates");
    if (flexTemplates) {
      flexTemplates.forEach((row) => {
        FlexTemplates.insert(row);
      });
    }

    if (!existingData || !existingData.length) {
      fetchDynamicTableData.call((err, res) => {
        if (err) {
          Notifications.error("Couldn't fetch data", err.reason, { timeout: 5000 });
        }
        else if (Meteor.isClient) {
          Session.setPersistent("dynamicTableData", res);
          res.forEach((row) => {
            TempDynamicTableData.insert(row);
          });
          this._isReady.set(true);
        }
      });
    }
    else {
      existingData.forEach((row) => {
        TempDynamicTableData.insert(row);
      });
      this._isReady.set(true);
    }
    if (Meteor.isClient) {
      this.autorun(() => {
        Session.setPersistent("dynamicTableData", TempDynamicTableData.find().fetch());
      });

      this.autorun(() => {
        Session.setPersistent("dynamicTableSpecs", DynamicTableSpecs.find().fetch());
      });
      this.autorun(() => {
        Session.setPersistent("flexTemplates", FlexTemplates.find().fetch());
      });
    }
  }

  destructor() {
    if (Meteor.isClient) {
      DynamicTableSpecs.remove({});
      FlexTemplates.remove({});
      TempDynamicTableData.remove({});
    }
  }

  tableSpec() {
    return _tableSpec;
  }

  availableColumns() {
    const gpCol = _tableSpec.columns;
    return () => {
      const ft = Tracker.nonreactive(() => FlexTemplates.findOne());
      if (ft && ft.fields) {
        return _.union(
          gpCol,
          ft.fields.map(field => ft.flexColumnForField(field, TempDynamicTableData, undefined))
        );
      }
      return gpCol;
    };
  }

  groupableFields() {
    const ft = FlexTemplates.findOne();
    const standardFields = [
      {
        label: "Category",
        field: "category",
        count: true
      }
    ];
    const flexFields = ft ? ft.groupableFields({}) : [];
    return _.union(standardFields, flexFields || []);
  }

  isReady() {
    return this._isReady.get();
  }

  savedTableSpecFn() {
    return DynamicTableSpec.customTableSpecFn("demo-table");
  }

  manageFieldsOptions() {
    const ft = FlexTemplates.findOne();
    if (!ft) {
      return FlexTemplate.manageFieldOptions({}, TempDynamicTableData);
    }
    return ft.manageFieldsOptions(TempDynamicTableData);
  }
}

BlazeComponent.register(Template.dynamicTables, DynamicTablesComponent);
