import _ from "underscore";
import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { Notifications } from "meteor/gfk:notifications";
import { MeteorPrincipalProvider } from "meteor/znewsham:encrypt";
import { EncryptedMessages } from "../../../collections/encryptedMessages/encryptedMessages.js";
import "./encryptedMessages.html";

export class EncryptedMessagesComponent extends BlazeComponent {
  static HelperMap() {
    return [
      "messages",
      "getMessage",
      "isEncrypted"
    ];
  }

  static EventMap() {
    return {
      "click button": "addMessage",
      "click .show-message": "showMessage",
      "click .request-access": "requestAccess",
      "click .access-requests": "grantAccess"
    };
  }

  init() {
    this.decryptedMessages = new ReactiveVar([]);
    if (Meteor.isServer) {
      return;
    }
    this.principalProvider = new MeteorPrincipalProvider();
    this.autorun((comp) => {
      if (!Meteor.user()) {
        return;
      }
      const password = window.localStorage.getItem("password");
      if (password) {
        this.principalProvider.init(Meteor.user().masterPrincipalId, password);
        comp.stop();
      }
    });
    this.subscribe("encryptedMessages");

    this.autorun(async () => {
      this.principalProvider.ready();
      const messages = EncryptedMessages.find().fetch();
      for (let i = 0; i < messages.length; i++) {
        const message = messages[i];
        const principalId = message._enc && message._enc._;
        if (!principalId || !this.principalProvider._userPrincipal) {
          continue;
        }
        const principals = await this.principalProvider.getPath(this.principalProvider._userPrincipal, principalId);

        if (!principals || principals.length === 0) {
          continue;
        }
        const principal = principals.slice(-1)[0];
        if (!principal.isUnlocked()) {
          continue;
        }
        await principal.init();
        try {
          message.message = await principal.decrypt(message.message);
          message.date = new Date(await principal.decrypt(message.date));
          message.accessRequests = await this.principalProvider.getAccessRequests(message._enc._);
        }
        catch (e) {
          console.error(e);
        }
      }
      this.decryptedMessages.set(messages);
    });
  }

  isEncrypted(message) {
    return !_.isString(message.message);
  }

  getMessage(message) {
    if (_.isString(message)) {
      return message;
    }
    return message.join(", ");
  }

  messages() {
    return this.decryptedMessages.get();
  }

  showMessage(e) {
    e.preventDefault();
    $(e.currentTarget).closest(".card").find(".card-footer,.card-text").toggle();
  }

  async addMessage() {
    if (!this.principalProvider._userPrincipal) {
      const password = window.prompt("You need to enter a password");
      await this.principalProvider.init(Meteor.user().masterPrincipalId, password);
      window.localStorage.setItem("password", password);
    }
    try {
      const messagePrincipal = await this.principalProvider.newEncryptionPrincipal();
      await this.principalProvider.storePrincipal(messagePrincipal);
      EncryptedMessages.insert({
        _enc: {
          _: messagePrincipal._id
        },
        name: this.$(".name").val(),
        message: await messagePrincipal.encrypt(this.$(".message").val()),
        date: await messagePrincipal.encrypt(new Date().toString())
      });
    }
    catch (e) {
      console.error(e);
    }
  }

  async requestAccess(e) {
    if (!this.principalProvider._userPrincipal) {
      const password = window.prompt("You need to enter a password");
      await this.principalProvider.init(Meteor.user().masterPrincipalId, password);
      window.localStorage.setItem("password", password);
    }
    try {
      this.principalProvider.requestAccess(this.principalProvider._userPrincipal._id, $(e.currentTarget).data("principalId"));
      Notifications.success("Requested Access", "", { timeout: 2000 });
    }
    catch (e1) {
      Notifications.error("Couldn't request access", e1.message, { timeout: 5000 });
    }
  }

  async grantAccess(e) {
    const pp = this.principalProvider;
    const message = this.decryptedMessages.get().find(m => m._id === $(e.currentTarget).data("messageId"));

    const principal = await pp.getPrincipal(message._enc._);

    for (let i = 0; i < message.accessRequests.length; i++) {
      const requestingPrincipal = await pp.getPrincipal(message.accessRequests[i].fromPrincipalId);
      const ar = await principal.grantAccessFor(requestingPrincipal);
      pp.saveAccess(requestingPrincipal._id, ar);
    }
  }
}

BlazeComponent.register(Template.encryptedMessages, EncryptedMessagesComponent);
