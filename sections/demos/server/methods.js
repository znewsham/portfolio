import { Articles } from "/sections/articles/collections/article/article.js";

export function fetchDynamicTableData() {
  return Articles.find({}, { fields: { title: 1, slug: 1, description: 1, imageSrc: 1, tags: 1, lastEdited: 1 } }).fetch();
}
