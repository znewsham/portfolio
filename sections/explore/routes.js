import { FlowRouter } from "meteor/kadira:flow-router";

FlowRouter.route("/explore/:collectionName?/:componentName?/:caseName?", {
  routeLoading: false,
  name: "explore",
  action({ collectionName, componentName, caseName }) {
    return Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("/ui/layouts/main/main.js"),
      import("./components.js"),
      import("meteor/znewsham:blaze-explorer")
    ])
    .then(([{ BlazeLayout }]) => {
      Meteor.RouteLoading.set(false);
      return BlazeLayout.render(
        "mainLayout",
        {
          main: "__blazeExplorer_main",
          container: false,
          style: "padding-top: 56px; margin-top: -56px; height: 100%;",
          data: {
            otherData: "test",
            collectionName,
            componentName,
            caseName
          }
        }
      );
    });
  }
});
