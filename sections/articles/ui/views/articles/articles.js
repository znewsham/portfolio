import moment from "moment";
import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
import { Articles } from "../../../collections/article/article.js";
import "./articles.html";

export class ArticlesView extends BlazeComponent {
  static HelperMap() {
    return [
      "articles",
      "imageSrc",
      "lastUpdated"
    ];
  }

  init() {
    this.autorun(() => {
      const tag = this.reactiveData("tag").tag;
      if (tag) {
        this.subscribe("articles", tag, () => {
          Meteor.RouteLoading && Meteor.RouteLoading.set(false);
          Tracker.afterFlush(() => {
            BlazeLayout.initialRenderComplete(this.templateInstance);
          });
        });
      }
    });
  }

  rendered() {
  }

  articles() {
    return Articles.find(
      { tags: this.nonReactiveData().tag },
      {
        fields: {
          slug: 1, imageSrc: 1, title: 1, description: 1, lastEdited: 1
        },
        sort: { lastEdited: 1 }
      }
    ).fetch();
  }

  lastUpdated(article) {
    return moment(article.lastEdited).fromNow();
  }

  imageSrc(article) {
    return article.imageSrc || "/placeholder.png";
  }
}


BlazeComponent.register(Template.articles, ArticlesView);
