import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { BlockList } from "meteor/znewsham:article-editor";
import { Session } from "meteor/session";
import "meteor/znewsham:article-editor/imports/ui/components/viewArticle/viewArticle.js";
import "./previewArticle.html";
import "/sections/articles/setupArticleEditor.js";

export class ArticlePreView extends BlazeComponent {
  static HelperMap() {
    return [
      "blockList"
    ];
  }

  init() {
    this._blockList = new ReactiveVar(new BlockList([]));

    if (Meteor.isClient) {
      this.autorun(() => {
        const blocks = Session.get("preview-article");
        if (blocks && blocks.length) {
          Meteor.RouteLoading && Meteor.RouteLoading.set(false);
          this._blockList.get().setBlocks(blocks);
        }
      });
    }
  }

  blockList() {
    return this._blockList.get();
  }
}


BlazeComponent.register(Template.previewArticle, ArticlePreView);
