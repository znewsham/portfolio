import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { BlockList } from "meteor/znewsham:article-editor";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
import "meteor/znewsham:article-editor/imports/ui/components/viewArticle/viewArticle.js";
import "/ui/components/notFound/notFound.js";
import "/sections/articles/setupArticleEditor.js";
import "./viewArticle.html";

export class ArticleView extends BlazeComponent {
  static HelperMap() {
    return [
      "blockList"
    ];
  }

  init() {
    this._blockList = new BlockList([]);
  }

  rendered() {
    this.once(
      () => this.reactiveData("articleSlug"),
      (comp) => {
        if (Meteor.isClient && this._blockList.ready()) {
          Meteor.RouteLoading.set(false);
          window.scrollTo(0, 0);
          BlazeLayout.initialRenderComplete();
          comp.pause();
        }
      }
    );
    this.autorun(() => {
      this.reactiveData("articleSlug");
    });
  }

  blockList() {
    return this._blockList;
  }
}


BlazeComponent.register(Template.viewArticle, ArticleView);
