import "meteor/teamon:tinymce";
import { Notifications } from "meteor/gfk:notifications";
import _ from "underscore";
import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
import { BlockList, BlockFactory } from "meteor/znewsham:article-editor";
import "meteor/znewsham:article-editor/imports/ui/components/editArticle/editArticle.js";
import { Session } from "meteor/session";
import { Articles } from "../../../collections/article/article.js";
import "./editArticle.html";
import select2Init from "/vendors/select2.full.min.js";
import "/vendors/select2.min.css";
import "/sections/articles/setupArticleEditor.js";

if (Meteor.isClient) {
  select2Init(window, jQuery);
}

export class EditArticleView extends BlazeComponent {
  static HelperMap() {
    return [
      "blocks",
      "blockList",
      "isSaveDisabled",
      "article"
    ];
  }

  static EventMap() {
    return {
      "click .save": "saveArticle",
      "change #imageSrc": "fileToDataUri"
    };
  }

  init() {
    this._blockList = new BlockList([]);
  }

  rendered() {
    if (Meteor.isClient) {
      this.on("#preview-btn", "click", this.previewArticle.bind(this));
      this.on("#save-btn", "click", this.saveArticle.bind(this));
    }
    this.autorun((comp) => {
      if (this._blockList.ready()) {
        Meteor.RouteLoading && Meteor.RouteLoading.set(false);
        if (this._blockList.getBlocks().length === 0) {
          const block = BlockFactory.BlockFromObject({ type: "header" });
          block.markAsSaved();
          this._blockList.setBlocks([block]);
        }
        BlazeLayout.initialRenderComplete();
        comp.stop();
      }
    });

    if (Meteor.isClient) {
      this.$("#tags").select2({
        tags: true,
        multiple: true,
        data: ["research", "open-source", "work-experience", "personal-projects"].map(t => ({ text: t, id: t }))
      });
    }

    if (Meteor.isClient) {
      this.autorun(() => {
        this.subscribe("articleDetails", this.reactiveData("articleSlug").articleSlug);
      });

      this.autorun(() => {
        const article = Articles.findOne({ slug: this.reactiveData("articleSlug").articleSlug }, { fields: { tags: 1 } });
        if (article) {
          this.$("#tags").val(article.tags || []).trigger("change");
        }
      });
    }
  }

  previewArticle() {
    Session.setPersistent("preview-article", this._blockList.toArray());
  }

  isSaveDisabled() {
    return !this._blockList.hasUnsavedChanges() ? { disabled: "disabled" } : {};
  }

  blockList() {
    return this._blockList;
  }

  article() {
    return Articles.findOne({ slug: this.reactiveData("articleSlug").articleSlug });
  }

  getExtra() {
    const extra = {};
    _.toArray(this.$("#article-extra")
    .find("input,select,textarea"))
    .forEach((elem) => {
      const $elem = $(elem);
      if (elem.type === "file") {
        const dataUri = $elem.data("uri");
        if (dataUri) {
          extra[$elem.attr("name")] = dataUri;
        }
      }
      else {
        extra[$elem.attr("name")] = $elem.val();
      }
    });

    extra.lastEdited = new Date();

    return extra;
  }

  fileToDataUri() {
    const file = $("#imageSrc")[0].files[0];
    const reader = new FileReader();

    reader.addEventListener("load", () => {
      $("#imageSrc").data("uri", reader.result);
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  saveArticle() {
    this._blockList.save(
      this.nonReactiveData().articleSlug,
      this.getExtra(),
      (err, res) => {
        if (err) {
          Notifications.error("Couldn't save article", err.reason, { timeout: 5000 });
        }
        else {
          Notifications.success("Article Saved", "", { timeout: 2000 });
        }
      }
    );
  }
}


BlazeComponent.register(Template.editArticle, EditArticleView);
