import { BlockFactory, Block } from "meteor/znewsham:article-editor";

export class CodeListingBlock extends Block {
  static _id() {
    return "codeListing";
  }

  static label() {
    return "Code Listing";
  }

  constructor(block) {
    if (!block) {
      block = {
        type: "codeListing",
        content: {
          url: ""
        }
      };
    }
    if (!block.options) {
      block.options = {};
    }
    if (!block.content) {
      block.content = {
        url: "",
        type: "remote"
      };
    }
    block.title = "Code Listing";
    super(block);
  }

  getEditorTemplate() {
    return "editCodeListing";
  }

  getViewerTemplate() {
    return "codeListing";
  }
}

BlockFactory.RegisterBlockType("codeListing", CodeListingBlock);
