import { BlazeComponent } from "meteor/znewsham:blaze-component";
import "./editCodeListing.html";

export class EditCodeListingComponent extends BlazeComponent {
  static HelperMap() {
    return [
      "content",
      "selected",
      "inline"
    ];
  }

  static EventMap() {
    return {
      "change input": "changeContent",
      "change select": "changeContent",
      "keyup input": "changeContent",
      "change textarea": "changeContent",
      "keyup textarea": "changeContent"
    };
  }

  init() {
  }

  inline() {
    const type = Tracker.guard(() => this.content(true).type);
    return type === "inline";
  }

  changeContent() {
    const content = {};
    const block = this.nonReactiveData().block;
    this.$("input,select,textarea").each(function eachInput() {
      const $this = $(this);
      const checked = !["radio", "checkbox"].includes($this.attr("type")) || $this.is(":checked");
      const val = $this.val();
      if (val && checked) {
        const name = $this.attr("data-name") ? $this.attr("data-name") : $this.attr("name");
        content[name] = $this.attr("type") === "number" ? parseInt(val, 10) : val;
      }
    });
    this.localChanges = true;
    block.setContent(content);
    Tracker.afterFlush(() => {
      this.localChanges = false;
    });
  }

  content(force) {
    if (force !== true && this.localChanges) {
      return this.lastContent;
    }
    const block = this.nonReactiveData().block;
    block.depend();
    this.lastContent = block.getContent();
    return this.lastContent;
  }

  selected(field, value, name = "selected") {
    const fieldValue = Tracker.guard(() => this.content(true)[field]);
    // NOTE: blaze gives the template as the last argument
    if (typeof name !== "string") {
      name = "selected";
    }
    return fieldValue === value ? { [name]: name } : {};
  }
}

BlazeComponent.register(Template.editCodeListing, EditCodeListingComponent);
