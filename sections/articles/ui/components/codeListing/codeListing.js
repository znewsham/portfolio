let Prism;

if (Meteor.isClient) {
  // NOTE: to ensure we don't trigger the babel plugin on the server
  import P from "./prism.js"
  Prism = P;
  import "prism-github";
}

if (Meteor.isServer) {
  import HTTP from "meteor/http";
}

import { BlazeComponent } from "meteor/znewsham:blaze-component";
import "./codeListing.html";

function _fetch(url) {
  if (Meteor.isClient) {
    return fetch(url)
    .then(res => res.body.getReader().read())
    .then(res => new TextDecoder("utf-8").decode(res.value));
  }
  return new Promise((resolve, reject) => {
    HTTP.get(url, (err, res) => {
      if (err) {
        reject(err);
      }
      else {
        resolve(res.content);
      }
    });
  });
}

export class CodeListingComponent extends BlazeComponent {
  static HelperMap() {
    return [
      "code",
      "getStartLineNumber",
      "content"
    ];
  }

  init() {
    this._code = new ReactiveVar("");
    const block = this.nonReactiveData().block;
    this.autorun(() => {
      block.depend();
      const content = block.getContent();
      if (content.url) {
        _fetch(content.url)
        .then(res => this._code.set(res));
      }
      else {
        this._code.set(content.code);
      }
    });
  }

  content() {
    const block = this.nonReactiveData().block;
    block.depend();
    const content = block.getContent();
    return content;
  }

  code(block) {
    block.depend();
    const content = block.getContent();
    const startLineNumber = content.startLine || 1;
    const endLineNumber = content.endLine || undefined;
    const code = this._code.get().split("\n").slice(startLineNumber - 1, endLineNumber).join("\n");

    if (code && Prism) {
      Tracker.afterFlush(() => {
        Prism.highlightAll();
      });
    }
    return code;
  }

  getStartLineNumber(block) {
    block.depend();
    const content = block.getContent();
    return (content.startLine || 1);
  }
}

BlazeComponent.register(Template.codeListing, CodeListingComponent);
