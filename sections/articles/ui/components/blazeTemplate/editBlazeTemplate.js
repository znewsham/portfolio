import { BlazeComponent } from "meteor/znewsham:blaze-component";
import "./editBlazeTemplate.html";

export class EditBlazeTemplateComponent extends BlazeComponent {
  static HelperMap() {
    return [
      "content",
      "selected"
    ];
  }

  static EventMap() {
    return {
      "change input": "changeContent",
      "change select": "changeContent",
      "keyup input": "changeContent"
    };
  }

  changeContent() {
    const content = {};
    const block = this.nonReactiveData().block;
    this.$("input,select").each(function eachInput() {
      const $this = $(this);
      const val = $this.val();
      if (val) {
        content[$this.attr("name")] = $this.attr("type") === "number" ? parseInt(val, 10) : val;
      }
    });
    block.setContent(content);
  }

  content() {
    const block = this.nonReactiveData().block;
    block.depend();
    return block.getContent();
  }

  selected(language) {
    return this.content().languge === language ? { selected: "selected" } : {};
  }
}

BlazeComponent.register(Template.editBlazeTemplate, EditBlazeTemplateComponent);
