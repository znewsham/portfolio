import { BlockFactory, Block } from "meteor/znewsham:article-editor";

export class BlazeTemplateBlock extends Block {
  static _id() {
    return "blazeTemplate";
  }

  static label() {
    return "Blaze Template";
  }

  constructor(block) {
    if (!block) {
      block = {
        type: "blazeTemplate",
        content: {
          templateName: ""
        }
      };
    }
    if (!block.options) {
      block.options = {};
    }
    if (!block.content) {
      block.content = {
        templateName: ""
      };
    }
    block.title = "Blaze Template";
    super(block);
  }

  getEditorTemplate() {
    return "editBlazeTemplate";
  }

  getViewerTemplate() {
    return "blazeTemplate";
  }
}

BlockFactory.RegisterBlockType("blazeTemplate", BlazeTemplateBlock);
