import { BlockFactory, Block } from "meteor/znewsham:article-editor";

export class HiveBoardBlock extends Block {
  static _id() {
    return "hiveBoard";
  }

  static label() {
    return "Hive Board";
  }

  constructor(block) {
    if (!block) {
      block = {
        type: "hiveBoard",
        content: ""
      };
    }
    if (!block.options) {
      block.options = {};
    }
    if (!block.content) {
      block.content = "";
    }
    block.title = "Hive Board";
    super(block);
  }

  getEditorTemplate() {
    return "editHiveBoard";
  }

  getViewerTemplate() {
    return "hiveBoard";
  }
}

BlockFactory.RegisterBlockType("hiveBoard", HiveBoardBlock);
