import _ from "underscore";
import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { Tracker } from "meteor/tracker";
import { Hexagon } from "/sections/hive/ui/hexagon.js";
import HiveGame from "/api/games_ai/hive/hive-game.js";
import HiveState from "/api/games_ai/hive/hive-state.js";
import HivePlayer from "/api/games_ai/hive/hive-player.js";
import ManualActionChooser from "/api/games_ai/common/manual-chooser.js";
import allowedActions from "/api/games_ai/hive/allowedActions.js";

import "./hiveBoard.html";
import "/sections/hive/ui/components/hiveGameBoard/hiveGameBoard.js";

const moveMatcher = /([01])([hasb][1-3]|q)(n?[0-9][0-9])(n?[0-9][0-9])(n?[0-9][0-9])/;

export class HiveBoardComponent extends BlazeComponent {
  static HelperMap() {
    return [
      "size",
      "visibleHexagons",
      "boardDimensions",
      "game",
      "currentMove",
      "numMoves",
      "playing"
    ];
  }

  static EventMap() {
    return {
      "click .play": "playPauseGame",
      "click .previous-move": "previousMove",
      "click .next-move": "nextMove",
      "change .current-move": "changeMove"
    };
  }

  init() {
    this._hexagons = new Map();
    this._visibleHexagons = new Tracker.Dependency();
    this._game = new HiveGame();
    this._gameChanged = new Tracker.Dependency();
    this._playing = new ReactiveVar(false);
    this._allMoves = [];
    this._currentMove = this._allMoves.length - 1;
    const block = this.nonReactiveData().block;
    this.autorun(() => {
      block.depend();
      const content = block.getContent();
      let moves;
      if (content.trim().startsWith("[")) {
        moves = eval(content);
      }
      else {
        moves = content.split(" ")
        .map(move => move.trim())
        .map((move) => {
          const match = move.match(moveMatcher);
          if (!match) {
            console.error("Bad move");
            return null;
          }
          return {
            playerId: `${match[1]}`,
            pieceId: `${match[1]}-${match[2]}`,
            locationId: `${parseInt(match[3].replace("n", "-"), 10)},${parseInt(match[4].replace("n", "-"), 10)},${parseInt(match[5].replace("n", "-"), 10)}`
          };
        });
      }
      this._currentMove = moves.length - 1;
      this._allMoves = moves.slice(0, moves.length - 1);
      this.setupGame(moves);
    });
  }

  setupGame(moves) {
    this._game.state = new HiveState();
    this._game.players = [];
    this._game.player1 = null;
    this._game.player2 = null;
    this._gameChanged.changed();
    const locations = Array.from(this._game.state.locations.values());
    locations.forEach((location) => {
      this._hexagons.set(location.id(), new Hexagon(location));
    });
    moves.forEach((move) => {
      let player = this._game.players.find(p => p._id === move.playerId);
      if (!player) {
        player = new HivePlayer(move.playerId, new ManualActionChooser(allowedActions));
        this._game.addPlayer(player);
      }
      const piece = this._game.getPiece(move.pieceId);
      const location = this._game.getLocation(move.locationId);
      const availableActions = player.actionChooser().allowedActions(player, this._game.state);
      if (!availableActions.find(a => a.piece === piece && a.location === location)) {
        console.log(availableActions);
        console.error("Illegal move", piece, location);
      }
      this._game.state.action(player, { piece, location });
    });
    this._visibleHexagons.changed();
  }

  playing() {
    return this._playing.get();
  }

  numMoves() {
    this._visibleHexagons.depend();
    return this._allMoves.length;
  }

  currentMove() {
    this._visibleHexagons.depend();
    return this._currentMove;
  }

  boardDimensions() {
    const size = this.size();
    this._visibleHexagons.depend();
    const game = this._game;
    let minTop = 1000000;
    let minLeft = 1000000;
    let maxLeft = 0;
    let maxTop = 0;
    game.state.placedPieces.forEach((piece) => {
      minTop = Math.min(piece.location.hexagon.middle(size).y, minTop);
      minLeft = Math.min(piece.location.hexagon.middle(size).x, minLeft);
      maxTop = Math.max(piece.location.hexagon.middle(size).y, maxTop);
      maxLeft = Math.max(piece.location.hexagon.middle(size).x, maxLeft);
    });
    if (game.state.placedPieces.length === 0) {
      minTop = 0;
      minLeft = 0;
    }
    maxTop += (size * 3);
    maxLeft += (size * Math.sqrt(3) * 1.5);
    const bounds = {
      top: minTop - (size * 3),
      left: minLeft - (size * Math.sqrt(3) * 1.5)
    };

    bounds.width = maxLeft - bounds.left;
    bounds.height = maxTop - bounds.top;

    return bounds;
  }

  game() {
    this._gameChanged.depend();
    return this._game;
  }

  size() {
    return 20;
  }

  hexagons() {
    return Array.from(this._hexagons.values());
  }

  visibleHexagons() {
    this._visibleHexagons.depend();
    let hasPiece = this.hexagons().filter(h => h.location.pieces.length);
    if (hasPiece.length === 0) {
      hasPiece = this.hexagons().slice(0, 1);
    }
    return _.union(hasPiece, _.unique(_.flatten(hasPiece.map(piece => piece.location.neighbours.map(l => l.hexagon)))));
  }

  changeMove(e) {
    const move = parseInt($(e.currentTarget).val(), 10);
    this._currentMove = move;
    this.setupGame(this._allMoves.slice(0, move));
  }

  previousMove() {
    if (this._currentMove > 0) {
      this.setupGame(this._allMoves.slice(0, --this._currentMove));
    }
  }

  nextMove() {
    if (this._currentMove < this._allMoves.length - 1) {
      this.setupGame(this._allMoves.slice(0, ++this._currentMove));
    }
  }

  playPauseGame() {
    const playing = !this._playing.get();
    this._playing.set(playing);
    if (!playing) {
      this.clearInterval(this.playInterval);
    }
    else {
      this.playInterval = this.setInterval(() => {
        if (this._currentMove >= this._allMoves.length) {
          this.clearInterval(this.playInterval);
          this._playing.set(false);
        }
        else {
          this.setupGame(this._allMoves.slice(0, ++this._currentMove));
        }
      }, 500);
    }
  }
}

BlazeComponent.register(Template.hiveBoard, HiveBoardComponent);
