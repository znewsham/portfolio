import { BlazeComponent } from "meteor/znewsham:blaze-component";
import "./editHiveBoard.html";

export class EditHiveBoardComponent extends BlazeComponent {
  static HelperMap() {
    return [
      "content"
    ];
  }

  static EventMap() {
    return {
      "change textarea": "changeContent",
      "keyup textarea": "changeContent"
    };
  }

  changeContent() {
    const block = this.nonReactiveData().block;
    const content = this.$("textarea").val();
    this.localChanges = true;
    block.setContent(content);
    Tracker.afterFlush(() => {
      this.localChanges = false;
    });
  }

  content(force) {
    if (force !== true && this.localChanges) {
      return this.lastContent;
    }
    const block = this.nonReactiveData().block;
    block.depend();
    this.lastContent = block.getContent();
    return this.lastContent;
  }
}

BlazeComponent.register(Template.editHiveBoard, EditHiveBoardComponent);
