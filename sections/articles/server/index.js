import { config } from "meteor/znewsham:article-editor";
import { Articles } from "../collections/article/article.js";
import "../collections/article/publications.js";
import "../collections/article/permissions.js";
import "./methods.js";

Meteor.startup(() => {
  config({
    collection: Articles
  });
});
