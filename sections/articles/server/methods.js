import cheerio from "cheerio";
import { Articles } from "../collections/article/article.js";

const splitterRegex = /[^a-zA-Z-]+/;
Meteor.methods({
  async getUniqueArticleWords() {
    const blocks = await Articles.rawCollection().aggregate([
      { $unwind: "$blocks" },
      { $match: { "blocks.type": "WYSIWYG" } },
      { $project: { content: "$blocks.content" } }
    ]).toArray();

    const words = _.flatten(blocks.map((b) => {
      const $ = cheerio.load(`<root>${b.content}</root>`);
      return $("root").text().split(splitterRegex);
    })).filter(w => w.length > 1);

    const counts = words.reduce((memo, word) => {
      if (memo[word]) {
        memo[word].count++;
      }
      else {
        memo[word] = {
          word,
          count: 1
        };
      }
      return memo;
    }, {});
    return _.values(counts);
  }
});
