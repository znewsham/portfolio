import { config } from "meteor/znewsham:article-editor";
import { Articles } from "./collections/article/article.js";

import "./ui/components/codeListing/codeListing.js";
import "./ui/components/codeListing/editCodeListing.js";
import "./ui/components/codeListing/CodeListingBlock.js";

import "./ui/components/blazeTemplate/blazeTemplate.js";
import "./ui/components/blazeTemplate/editBlazeTemplate.js";
import "./ui/components/blazeTemplate/BlazeTemplateBlock.js";

import "./ui/components/hiveBoard/hiveBoard.js";
import "./ui/components/hiveBoard/editHiveBoard.js";
import "./ui/components/hiveBoard/HiveBoardBlock.js";

Meteor.startup(() => {
  config({
    collection: Articles
  });
});
