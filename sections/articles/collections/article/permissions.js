import { Articles } from "./article.js";

Articles.allow({
  update() {
    return true; // NOTE: rely on deny
  },
  insert() {
    return true; // NOTE: rely on deny
  },
  remove() {
    return false;
  }
});

Articles.deny({
  insert(userId) {
    const user = Meteor.users.findOne(
      { _id: userId },
      { fields: { isAdmin: 1 } }
    );
    return !(user && user.isAdmin);
  },
  update(userId) {
    const user = Meteor.users.findOne(
      { _id: userId },
      { fields: { isAdmin: 1 } }
    );
    return !(user && user.isAdmin);
  },
  remove() {
    return true;
  }
});
