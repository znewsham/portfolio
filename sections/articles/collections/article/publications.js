import { check } from "meteor/check";
import { Articles } from "./article.js";

function articles(tag) {
  check(tag, String);
  return Articles.find(
    { tags: tag },
    {
      fields: {
        imageSrc: 1, tags: 1, lastEdited: 1, title: 1, description: 1, slug: 1
      },
      limit: 10
    }
  );
}

function articleDetails(slug) {
  check(slug, String);
  return Articles.find(
    { slug },
    { fields: { blocks: false }, limit: 1 }
  );
}

Meteor.publish("articles", articles);
Meteor.publish("articleDetails", articleDetails);
