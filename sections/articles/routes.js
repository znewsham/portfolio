import { FlowRouter } from "meteor/kadira:flow-router";
import { Articles } from "./collections/article/article.js";

const ArticleRoutes = FlowRouter.group({
  prefix: "/article",
  name: "article"
});

function cleanTag(tag) {
  return tag.split("-").map(p => p.slice(0, 1).toUpperCase() + p.slice(1)).join(" ");
}
FlowRouter.route("/articles/:tag", {
  name: "article",
  bitbucketUrl() {
    return `${Meteor.bitbucketRoot}/sections/articles/ui/views/articles`;
  },
  title() {
    return `Zack Newsham / ${cleanTag(FlowRouter.getParam("tag"))} Articles`;
  },
  action({ tag }) {
    return Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("./ui/views/articles/articles.js"),
      import("/ui/layouts/main/main.js")
    ])
    .then(([{ BlazeLayout }]) => {
      return BlazeLayout.render(
        "mainLayout",
        {
          main: "articles",
          data: { tag }
        }
      );
    });
  }
});

ArticleRoutes.route("/dynamic-tables", {
  name: "dynamicTables",
  action() {
    return Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("./ui/views/viewArticle/viewArticle.js"),
      import("/ui/layouts/main/main.js"),
      import("/sections/demos/ui/components/dynamicTables/dynamicTables.js")
    ])
    .then(([{ BlazeLayout }]) => {
      return BlazeLayout.render(
        "mainLayout",
        {
          main: "viewArticle",
          data: { articleSlug: "dynamic-tables" },
          footerData: { items: [{ text: "Edit", href: "/article/edit/dynamic-tables" }] }
        }
      );
    });
  }
});

ArticleRoutes.route("/encryption", {
  name: "encryption",
  action() {
    return Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("./ui/views/viewArticle/viewArticle.js"),
      import("/ui/layouts/main/main.js"),
      import("/sections/demos/ui/components/encryptedMessages/encryptedMessages.js")
    ])
    .then(([{ BlazeLayout }]) => {
      return BlazeLayout.render(
        "mainLayout",
        {
          main: "viewArticle",
          data: { articleSlug: "encryption" },
          footerData: { items: [{ text: "Edit", href: "/article/edit/encryption" }] }
        }
      );
    });
  }
});

ArticleRoutes.route("/:articleSlug", {
  name: "article",
  bitbucketUrl() {
    return `${Meteor.bitbucketRoot}/sections/articles/ui/views/viewArticle`;
  },
  title() {
    const articleSlug = FlowRouter.getParam("articleSlug");
    const article = Articles.findOne({ slug: articleSlug }, { fields: { title: 1 } });
    return `Zack Newsham / ${article ? article.title : "Article"}`;
  },
  action(params) {
    return Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("./ui/views/viewArticle/viewArticle.js"),
      import("/ui/layouts/main/main.js")
    ])
    .then(([{ BlazeLayout }]) => {
      const articleSlug = params.articleSlug;
      return BlazeLayout.render(
        "mainLayout",
        {
          main: "viewArticle",
          data: { articleSlug },
          footerData: { items: [{ text: "Edit", href: `/article/edit/${articleSlug}` }] }
        }
      );
    });
  }
});

ArticleRoutes.route("/preview/:articleSlug", {
  name: "preview",
  bitbucketUrl() {
    return `${Meteor.bitbucketRoot}/sections/articles/ui/views/previewArticle`;
  },
  title() {
    return "Zack Newsham / Article";
  },
  action() {
    return Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("./ui/views/previewArticle/previewArticle.js"),
      import("/ui/layouts/main/main.js")
    ])
    .then(([{ BlazeLayout }]) => {
      return BlazeLayout.render(
        "mainLayout",
        {
          main: "previewArticle"
        }
      );
    });
  }
});

if (Meteor.isClient) {
  ArticleRoutes.route("/edit/:articleSlug", {
    name: "article",
    bitbucketUrl() {
      return `${Meteor.bitbucketRoot}/sections/articles/ui/views/editArticle`;
    },
    title() {
      return "Zack Newsham / Edit Article";
    },
    action({ articleSlug }) {
      return Promise.all([
        import("meteor/kadira:blaze-layout"),
        import("/ui/layouts/main/main.js"),
        import("./ui/views/editArticle/editArticle.js"),
        Meteor.isClient ? import("/vendors/jquery-ui.min.js") : Promise.resolve()
      ])
      .then(([{ BlazeLayout }]) => {
        return BlazeLayout.render(
          "mainLayout",
          {
            main: "editArticle",
            data: { articleSlug },
            headerData: { saveBtn: true, previewBtn: true },
            footerData: { items: [{ text: "View", href: `/article/${articleSlug}` }] }
          }
        );
      });
    }
  });
}
