import { ValidatedMethod } from "meteor/mdg:validated-method";
import SimpleSchema from "simpl-schema";

let methodModule;
if (Meteor.isServer) {
  import * as mm from "./server/methods.js";

  methodModule = mm;
}
export const startGame = new ValidatedMethod({
  name: "hive.startGame",
  validate: new SimpleSchema({
    gameId: { type: String },
    against: { type: String }
  }).validator(),
  run({ gameId, against }) {
    if (Meteor.isServer) {
      return methodModule.startGame.call(this, gameId, against);
    }
  }
});

export const joinGame = new ValidatedMethod({
  name: "hive.joinGame",
  validate: new SimpleSchema({
    gameId: { type: String }
  }).validator(),
  run({ gameId }) {
    if (Meteor.isServer) {
      return methodModule.joinGame.call(this, gameId);
    }
  }
});

export const makeMove = new ValidatedMethod({
  name: "hive.makeMove",
  validate: new SimpleSchema({
    gameId: { type: String },
    pieceId: { type: String },
    locationId: { type: String }
  }).validator(),
  run({ gameId, pieceId, locationId }) {
    if (Meteor.isServer) {
      return methodModule.makeMove.call(this, gameId, pieceId, locationId);
    }
  }
});
