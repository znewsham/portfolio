import HiveGame from "/api/games_ai/hive/hive-game.js";
import HivePlayer from "/api/games_ai/hive/hive-player.js";
import ManualActionChooser from "/api/games_ai/common/manual-chooser.js";
import RandomActionChooser from "/api/games_ai/common/random-chooser.js";
import allowedActions from "/api/games_ai/hive/allowedActions.js";
import { Random } from "meteor/random";
import { Tracker } from "meteor/tracker";

export default class LocalHiveGame extends HiveGame {
  constructor(gameId, gameType, ...args) {
    super(...args);
    this.gameId = gameId || Random.id();
    this.gameType = gameType;
    this.dep = new Tracker.Dependency();
    this.addPlayer(new HivePlayer(Meteor.userId(), new ManualActionChooser(allowedActions)));
    this.addPlayer(new HivePlayer("random", new ManualActionChooser(allowedActions)));
    this.remotePlayerId = this.players[1]._id;
  }

  playWhenReady() {
    this.play(Math.round(Math.random()));
  }

  changed() {
    this.dep.changed();
  }

  depend() {
    this.dep.depend();
  }
}
