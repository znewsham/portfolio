import { FlowRouter } from "meteor/kadira:flow-router";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
import "/ui/layouts/main/main.js";
import "meteor/blaze";

const HiveRoutes = FlowRouter.group({
  prefix: "/hive",
  name: "hive"
});

HiveRoutes.route("/play/:gameId?", {
  name: "play",
  action() {
    import("./ui/views/hiveGame/hiveGame.js")
    .then(() => {
      BlazeLayout.render("mainLayout", { main: "hiveGameView" });
      Meteor.RouteLoading.set(false);
    });
  }
});
