import HiveGame from "/api/games_ai/hive/hive-game.js";
import HivePlayer from "/api/games_ai/hive/hive-player.js";
import ManualActionChooser from "/api/games_ai/common/manual-chooser.js";
import allowedActions from "/api/games_ai/hive/allowedActions.js";
import { Random } from "meteor/random";
import { Tracker } from "meteor/tracker";
import parseGame from "/api/games_ai/hive/parse_game_file.js";

const fileContent = ` (;
(;
GM[27]VV[1]
SU[hive]
DT[14. Januar 2018]
GN[HV-WeakBot-guest-2018-01-14-1539]
RE[WeakBot hat gewonnen]
P0[id "WeakBot"]
P1[id "guest"]
; P0[0 Start P0]
; P0[1 move W wB1 N 13 .]
; P0[2 done]
; P1[3 pick B 4 bS1]
; P1[4 dropb bS1 M 12 /wB1]
; P1[5 done]
; P0[6 move W wQ O 14 wB1/]
; P0[7 done]
; P1[8 pick B 1 bA1]
; P1[9 dropb bA1 L 12 -bS1]
; P1[10 done]
; P0[11 move W wQ O 13 wB1-]
; P0[12 done]
; P1[13 pick B 0 bQ]
; P1[14 dropb bQ M 11 bS1\\]
; P1[15 done]
; P0[16 move W wG1 P 14 wQ/]
; P0[17 done]
; P1[18 pickb L 12 bA1]
; P1[19 dropb bA1 Q 15 wG1/]
; P1[20 done]
; P0[21 move W wB2 O 14 \\wQ]
; P0[22 done]
; P1[23 pick B 1 bA2]
; P1[24 dropb bA2 L 12 -bS1]
; P1[25 done]
; P0[26 move W wS1 N 14 \\wB1]
; P0[27 done]
; P1[28 pickb L 12 bA2]
; P1[29 dropb bA2 N 15 \\wS1]
; P1[30 done]
; P0[31 move W wS2 P 13 wQ-]
; P0[32 done]
; P1[33 pick B 2 bG1]
; P1[34 dropb bG1 L 10 /bQ]
; P1[35 done]
; P0[36 move W wS2 N 11 bQ-]
; P0[37 done]
; P1[38 pickb L 10 bG1]
; P1[39 dropb bG1 N 12 /wQ]
; P1[40 done]
; P0[41 move W wS2 L 11 -bQ]
; P0[42 done]
; P1[43 pick B 4 bS2]
; P1[44 dropb bS2 M 10 bQ\\]
; P1[45 done]
; P0[46 move W wS2 M 9 bS2\\]
; P0[47 done]
; P1[48 pick B 2 bG2]
; P1[49 dropb bG2 L 12 -bS1]
; P1[50 done]
; P0[51 move W wB1 M 12 .]
; P0[52 done]
; P1[53 pickb L 12 bG2]
; P1[54 dropb bG2 O 12 wQ\\]
; P1[55 done]
; P0[56 move W wG2 M 13 \\wB1]
; P0[57 done]
; P1[58 pick B 2 bG3]
; P1[59 dropb bG3 N 16 \\bA2]
; P1[60 done]
; P0[61 move W wB1 L 11 -bQ]
; P0[62 done]
; P1[63 pickb N 16 bG3]
; P1[64 dropb bG3 N 13 -wQ]
; P1[65 done]
; P0[66 move W wB2 N 13 .]
; P0[67 done]
; P1[68 pick B 1 bA3]
; P1[69 dropb bA3 P 12 bG2-]
; P1[70 done]
; P0[71 move W wB2 N 12 .]
; P0[72 done]
; P1[73 pick B 3 bB1]
; P1[74 dropb bB1 O 11 /bA3]
; P1[75 done]
; P0[76 move W wG3 N 9 wS2-]
; P0[77 done]
; P1[78 pickb O 11 bB1]
; P1[79 dropb bB1 N 11 wB2\\]
; P1[80 done]
; P0[81 move W wG3 L 9 /bS2]
; P0[82 done]
; P1[83 pickb P 12 bA3]
; P1[84 dropb bA3 P 13 wQ-]
; P1[85 done]
; P0[86 move W wG2 O 15 bA2-]
; P0[87 done]
; P1[88 pickb N 15 bA2]
; P1[89 dropb bA2 P 16 wG2/]
; P1[90 done]
; P0[91 move W wG3 Q 14 bA1\\]
; P0[92 done]
; P1[93 pickb M 12 bS1]
; P1[94 dropb bS1 K 10 /wB1]
; P1[95 done]
; P0[96 move W wG1 L 10 /bQ]
; P0[97 done]
; P1[98 pickb O 12 bG2]
; P1[99 dropb bG2 O 14 \\wQ]
; P1[100 done]
; P0[101 move W wB2 M 12 \\bQ]
; P0[102 done]
;
P0[time 0:02:18 ]
P1[time 0:03:32 ]
)
 `;
const { game } = parseGame("/media/zacknewsham/a6b43774-038f-42c1-9367-a75843b18944/plays/regular/HV-guest-Dumbot-2010-02-02-1911.sgf", fileContent);
console.log(game);

export default class ReplayHiveGame extends HiveGame {
  constructor(gameId, gameType, ...args) {
    super(...args);
    this.gameId = gameId || Random.id();
    this.gameType = gameType;
    this.dep = new Tracker.Dependency();
    this.addPlayer(new HivePlayer(game.players[0]._id, new ManualActionChooser(allowedActions)));
    this.addPlayer(new HivePlayer(game.players[1]._id, new ManualActionChooser(allowedActions)));
    this.remotePlayerId = this.players[1]._id;
  }

  playWhenReady() {
    this.play(0);
    let currentAction = 0;
    setInterval(() => {
      const move = game.state.moves[currentAction++];
      const actualMove = { piece: this.getPiece(move.piece._id), location: this.getLocation(move.location.id()) };
      const allowedMoves = this.currentPlayer.actionChooser().allowedActions(this.currentPlayer, this.state);
      if (!allowedMoves.find(m => m.piece === actualMove.piece && m.location === actualMove.location)) {
        console.log("oh noo");
      }
      this.currentPlayer.actionChooser().takeAction(actualMove);
    }, 1000);
  }

  changed() {
    this.dep.changed();
  }

  depend() {
    this.dep.depend();
  }
}
