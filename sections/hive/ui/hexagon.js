import { Tracker } from "meteor/tracker";

export class Hexagon {
  constructor(location) {
    this.location = location;
    location.hexagon = this;
    this._highlighted = false;
    this._dependency = new Tracker.Dependency();
    this._id = location.id();
  }

  changed() {
    this._dependency.changed();
  }

  depend() {
    this._dependency.depend();
  }

  middle(size) {
    const hex = Hexagon.CubeToAxial(this.location);
    const x = size * (Math.sqrt(3) * hex.q + Math.sqrt(3) / 2 * hex.r);
    const y = size * (3.0 / 2 * hex.r);

    return { x, y };
  }

  highlighted() {
    this.depend();
    return this._highlighted;
  }

  scale(size) {
    return 1 / Hexagon.Size * size;
  }

  topPiece() {
    this.depend();
    return this.location.pieces.slice(-1)[0];
  }

  color(game) {
    this.depend();
    if (this.location.pieces.length === 0) {
      return "#cccccc";
    }
    return game.players.find(p => p._id === this.topPiece().playerId).color;
  }

  static CubeToAxial(cube) {
    return { q: cube.x, r: cube.z };
  }
}
Hexagon.Size = 14;
