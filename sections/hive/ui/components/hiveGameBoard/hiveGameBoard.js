import { BlazeComponent } from "meteor/znewsham:blaze-component";
import "./hiveGameBoard.html";
if (Meteor.isClient) {
  import "./hiveGameBoard.scss";
}
export class HiveGameBoardComponent extends BlazeComponent {
  static HelperMap() {
    return [
      "width",
      "height",
      "halfWidth",
      "halfHeight",
      "visibleHexagons"
    ];
  }

  height(size) {
    return size * 2;
  }

  halfHeight(size) {
    return size;
  }

  width(size) {
    return Math.sqrt(3) * size;
  }

  halfWidth(size) {
    return this.width(size) / 2;
  }

  // kinda hacky, but using visibleHexagons directly from the HTML will sometimes re-render the same elements twice,
  // but the game fails to find the player because the game is the one with no moves.
  visibleHexagons() {
    return this.nonReactiveData().visibleHexagons;
  }
}

BlazeComponent.register(Template.hiveGameBoard, HiveGameBoardComponent);
