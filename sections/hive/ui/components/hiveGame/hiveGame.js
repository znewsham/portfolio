import _ from "underscore";
import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { Notifications } from "meteor/gfk:notifications";
import { ReactiveVar } from "meteor/reactive-var";
import { Tracker } from "meteor/tracker";
import { Hexagon } from "../../hexagon.js";

import "../hiveGameBoard/hiveGameBoard.js";
import "./hiveGame.html";

export class HiveGameComponent extends BlazeComponent {
  static HelperMap() {
    return [
      "size",
      "palleteX",
      "palleteY",
      "unusedPieces",
      "visibleHexagons",
      "boardDimensions",
      "hasAllPlayers",
      "myTurn",
      "game",
      "player",
      "opponent",
      "youAreWinner",
      "youAreLoser",
      "winner",
      "hasPieces"
    ];
  }

  static EventMap() {
    return {
      "click .piece": "selectPiece",
      "click .gameBoard .hexagon": "selectMove"
    };
  }

  init() {
    this._hexagons = new Map();
    this._visibleHexagons = new Tracker.Dependency();
    this._availablePieces = new Tracker.Dependency();
    this._selectedPiece = new ReactiveVar();
    this._winner = new ReactiveVar(false);
    this.dataChangedStrict("game", ({ game }) => {
      const locations = Array.from(game.state.locations.values());
      locations.forEach((location) => {
        this._hexagons.set(location.id(), new Hexagon(location));
      });
      game.state.onAction((state, player, action) => {
        game.changed();
        if (action.oldLocation) {
          action.oldLocation.hexagon.changed();
        }
        action.location.hexagon.changed();
        this._visibleHexagons.changed();
      });
      game.playWhenReady()
      .then(() => {
        const winner = game.winner();
        this._winner.set(winner || true);
      })
      .catch((err) => {
        Notifications.error("Something went wrong :(", err.reason, { timeout: 5000 });
      });
    });
  }

  youAreWinner() {
    const winner = this.winner();
    return winner && winner._id === Meteor.userId();
  }

  youAreLoser() {
    const winner = this.winner();
    return winner && winner._id && winner._id !== Meteor.userId();
  }

  winner() {
    return this._winner.get();
  }

  game() {
    const game = this.nonReactiveData().game;
    game.depend();
    return game;
  }

  myTurn() {
    const game = this.nonReactiveData().game;
    game.depend();
    return game.currentPlayer._id !== game.remotePlayerId;
  }

  hasAllPlayers() {
    this.nonReactiveData().game.depend();
    return this.nonReactiveData().game.players.length === 2;
  }

  boardDimensions() {
    const size = this.size();
    this._visibleHexagons.depend();
    const game = this.nonReactiveData().game;
    let minTop = 1000000;
    let minLeft = 1000000;
    let maxLeft = 0;
    let maxTop = 0;
    game.state.placedPieces.forEach((piece) => {
      minTop = Math.min(piece.location.hexagon.middle(size).y, minTop);
      minLeft = Math.min(piece.location.hexagon.middle(size).x, minLeft);
      maxTop = Math.max(piece.location.hexagon.middle(size).y, maxTop);
      maxLeft = Math.max(piece.location.hexagon.middle(size).x, maxLeft);
    });
    if (game.state.placedPieces.length === 0) {
      minTop = 0;
      minLeft = 0;
    }
    maxTop += (size * 3);
    maxLeft += (size * Math.sqrt(3) * 1.5);
    const bounds = {
      top: minTop - (size * 3),
      left: minLeft - (size * Math.sqrt(3) * 1.5)
    };

    bounds.width = maxLeft - bounds.left;
    bounds.height = maxTop - bounds.top;

    return bounds;
  }

  palleteX(index) {
    return ((index % 2) * (24 + 6)) + 12;
  }

  palleteY(index) {
    return Math.floor(index / 2) * (24 + 6);
  }

  size() {
    return 20;
  }

  hexagons() {
    return Array.from(this._hexagons.values());
  }

  visibleHexagons() {
    this._visibleHexagons.depend();
    let hasPiece = this.hexagons().filter(h => h.location.pieces.length);
    if (hasPiece.length === 0) {
      hasPiece = this.hexagons().slice(0, 1);
    }
    return _.union(hasPiece, _.unique(_.flatten(hasPiece.map(piece => piece.location.neighbours.map(l => l.hexagon)))));
  }

  hasPieces(player) {
    return this.unusedPieces(player).length !== 0;
  }

  unusedPieces(player) {
    const game = this.nonReactiveData().game;
    game.depend();
    return player.pieces.filter(p => !p.location);
  }

  player() {
    const game = this.nonReactiveData().game;
    game.depend();
    return game.players.find(p => p._id === Meteor.userId());
  }

  opponent() {
    const game = this.nonReactiveData().game;
    game.depend();
    return game.players.find(p => p._id !== Meteor.userId());
  }


  selectPiece(e) {
    const pieceId = `${$(e.currentTarget).attr("data-pieceid")}`;
    const game = this.nonReactiveData().game;
    const piece = _.union(game.player1.pieces, game.player2.pieces).find(p => p._id === pieceId);
    if (!game.currentPlayer.pieces.includes(piece) || !game.currentPlayer.actionChooser().takeAction) {
      return;
    }
    const state = game.state;
    const selectedPiece = this._selectedPiece.get();
    if (piece && selectedPiece && selectedPiece.allowedMoves(state).includes(piece.location)) {
      return;
    }
    if (selectedPiece && selectedPiece !== piece) {
      selectedPiece.allowedMoves(state).forEach((location) => {
        location.hexagon._highlighted = false;
        location.hexagon.changed();
      });
    }
    if (piece) {
      const allowedMoves = piece.allowedMoves(state);
      allowedMoves.forEach((move) => {
        move.hexagon._highlighted = true;
        move.hexagon.changed();
      });
    }
    this._selectedPiece.set(piece);
  }

  selectMove(e) {
    const game = this.nonReactiveData().game;
    const hexagonId = `${$(e.currentTarget).attr("data-id")}`;
    const hexagon = this._hexagons.get(hexagonId);
    const selectedPiece = this._selectedPiece.get();
    if (!selectedPiece || selectedPiece.location === hexagon.location) {
      return;
    }
    if (!game.currentPlayer.pieces.includes(selectedPiece) || !game.currentPlayer.actionChooser().takeAction) {
      return;
    }
    game.currentPlayer.actionChooser().takeAction({ piece: selectedPiece, location: hexagon.location });
    const allowedMoves = selectedPiece.allowedMoves(game.state);
    allowedMoves.forEach((location) => {
      location.hexagon._highlighted = false;
      location.hexagon.changed();
    });
    this._selectedPiece.set(null);
  }
}

BlazeComponent.register(Template.hiveGameComponent, HiveGameComponent);
