import { BlazeLayout } from "meteor/kadira:blaze-layout";
import { BlazeComponent } from "meteor/znewsham:blaze-component";
import RemoteHiveGame from "../../../remote-hive-game.js";
import LocalHiveGame from "../../../local-hive-game.js";
//import ReplayHiveGame from "../../../replay-hive-game.js";

import "./hiveGame.html";
import "../../components/hiveGame/hiveGame.js";

export class HiveGameView extends BlazeComponent {
  static HelperMap() {
    return [
      "game",
      "needGameId"
    ];
  }

  static EventMap() {
    return {
      "change #game-type": "changeGameType",
      "click #start-game": "startGame"
    };
  }

  init() {
    this._game = new ReactiveVar();
  }

  rendered() {
    if (Meteor.isClient) {
      Meteor.RouteLoading.set(false);
      BlazeLayout.initialRenderComplete();
    }
  }

  game() {
    return this._game.get();
  }

  needGameId() {
    return this.get("gameType") === "join";
  }

  changeGameType() {
    this.set("gameType", this.$("#game-type").val());
  }

  startGame() {
    const gameType = this.get("gameType");
    if (gameType) {
      const gameId = this.$("#game-id").val();
      /*if (gameType === "replay") {
        const game = new ReplayHiveGame(gameId, gameType);
        this._game.set(game);
      }
      else*/ if (gameType !== "join" || gameId) {
        const game = gameType === "local" ? new LocalHiveGame(gameId, gameType) : new RemoteHiveGame(gameId, gameType);
        this._game.set(game);
      }
    }
  }
}
BlazeComponent.register(Template.hiveGameView, HiveGameView);
