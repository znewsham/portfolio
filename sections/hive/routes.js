import { FlowRouter } from "meteor/kadira:flow-router";
// import { BlazeLayout } from "meteor/kadira:blaze-layout";

const HiveRoutes = FlowRouter.group({
  prefix: "/hive",
  name: "hive"
});

HiveRoutes.route("/play/:gameId?", {
  name: "play",
  bitbucketUrl() {
    return `${Meteor.bitbucketRoot}/sections/hive/ui/views/hiveGame`;
  },
  title() {
    return `Zack Newsham / Play Hive`;
  },
  action() {
    return Promise.all([
      import("meteor/kadira:blaze-layout"),
      import("./ui/views/hiveGame/hiveGame.js"),
      import("/ui/layouts/main/main.js")
    ])
    .then(([{ BlazeLayout }]) => BlazeLayout.render("mainLayout", { main: "hiveGameView" }));
  }
});
