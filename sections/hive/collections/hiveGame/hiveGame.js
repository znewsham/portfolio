import { Mongo } from "meteor/mongo";
import HiveGame from "/api/games_ai/hive/hive-game.js";
import HiveGameSchema from "./schema.js";
import HivePlayer from "/api/games_ai/hive/hive-player.js";
import allowedActions from "/api/games_ai/hive/allowedActions.js";
import ManualActionChooser from "/api/games_ai/common/manual-chooser.js";
import RandomActionChooser from "/api/games_ai/common/random-chooser.js";

export class SavedHiveGame extends HiveGame {
  constructor(doc = {}) {
    super();
    this._id = doc._id;
    this.dateStarted = doc.dateStarted || new Date();
    (doc.players || []).forEach((player) => {
      // TODO: replace with factory
      const actualPlayer = new HivePlayer(player._id, player.actionChooser === "manual" ? new ManualActionChooser(allowedActions) : new RandomActionChooser(allowedActions));
      actualPlayer.connection = Meteor.default_server.sessions.get(player.connectionId);
      if (actualPlayer.connection) {
        actualPlayer.connection = actualPlayer.connection.connectionHandle;
      }
      this.players.push(actualPlayer);
    });
    this.currentPlayer = this.players.find(p => p._id === doc.currentPlayerId) || this.players[0];
    this.player1 = this.players[0];
    this.player2 = this.players[1];
    (doc.moves || []).forEach(move => this.state.action(
      this.players.find(p => p._id === move.playerId),
      { piece: this.getPiece(move.pieceId), location: this.getLocation(move.locationId) }
    ));
  }

  addPlayer(player) {
    if (!player.dateJoined) {
      player.dateJoined = new Date();
    }
    return super.addPlayer(player);
  }

  toJSONValue() {
    return {
      _id: this._id,
      currentPlayerId: this.currentPlayer && this.currentPlayer._id,
      players: this.players.map(p => ({
        _id: p._id,
        connectionId: p.connectionId,
        color: p.color,
        dateJoined: p.dateJoined,
        actionChooser: p.actionChooser().name()
      })),
      dateStarted: this.dateStarted,
      moves: this.state.moves.map(m => ({
        playerId: m.player._id,
        pieceId: m.piece._id,
        locationId: m.location.id()
      }))
    };
  }
}

export const SavedHiveGameMoves = new Mongo.Collection("hiveGameMoves");

export const SavedHiveGames = new Mongo.Collection("hiveGames", {
  transform(doc) {
    return new SavedHiveGame(doc);
  }
});
