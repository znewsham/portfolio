import SimpleSchema from "simpl-schema";

const HiveMoveSchema = new SimpleSchema({
  playerId: {
    type: String
  },
  pieceId: {
    type: String
  },
  locationId: {
    type: String
  }
});

const HiveWinnerSchema = new SimpleSchema({
  playerId: {
    type: String,
    optional: true // in case of draw
  },
  starter: {
    type: Boolean
  }
});

const HivePlayerSchema = new SimpleSchema({
  actionChooser: {
    type: String
  },
  connectionId: {
    type: String
  },
  _id: {
    type: String
  },
  color: {
    type: String
  },
  dateJoined: {
    type: String
  }
});

export default new SimpleSchema({
  _id: {
    type: String
  },
  currentPlayerId: {
    type: String
  },
  winner: {
    type: HiveWinnerSchema,
    optional: true
  },
  moves: {
    type: Array
  },
  "moves.$": {
    type: HiveMoveSchema
  },
  players: {
    type: Array
  },
  "players.$": {
    type: HivePlayerSchema
  },
  dateStarted: {
    type: Date,
    autoValue() {
      return new Date();
    }
  }
});
