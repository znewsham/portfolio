import Redis from "meteor/cultofcoders:redis-oplog";
import NetworkHivePlayer from "./network-hive-player.js";
import HivePlayer from "/api/games_ai/hive/hive-player.js";
import ManualActionChooser from "/api/games_ai/common/manual-chooser.js";
import RandomActionChooser from "/api/games_ai/common/random-chooser.js";
import allowedActions from "/api/games_ai/hive/allowedActions.js";
import { SavedHiveGames, SavedHiveGame } from "../collections/hiveGame/hiveGame.js";

const Vent = Redis.Vent;

const gamesInProgress = {};

Meteor.gamesInProgress = gamesInProgress;


Vent.publish("hiveGameEvents", function gameEvents(gameId, playerColor) {
  this.on(`hiveGame::${gameId}::${playerColor}::event`, event => event);
});

export function takeAction(game, player, pieceId, locationId, driver = false) {
  const piece = game.getPiece(pieceId);
  const location = game.getLocation(locationId);
  if (!piece) {
    throw new Meteor.Error("Invalid piece");
  }
  if (!location) {
    throw new Meteor.Error("Invalid location");
  }
  if (driver) {
    player.actionChooser().takeAction({ piece, location });
  }
  else {
    game.state.action(player, { piece, location });
  }
}

async function computersMove(game, computer) {
  const opponent = game.getOpponent(computer);
  const action = await computer.actionChooser().chooseAction(computer, game.state);
  takeAction(game, computer, action.piece._id, action.location.id(), false);
  SavedHiveGames.update(
    { _id: game._id },
    { $set: { currentPlayerId: opponent._id }, $push: { moves: { playerId: computer._id, pieceId: action.piece._id, locationId: action.location.id() } } }
  );
  Vent.emit(`hiveGame::${game._id}::${opponent._id}::event`, {
    type: "action", playerId: computer._id, pieceId: action.piece._id, locationId: action.location.id()
  });
}

export function startGame(gameId, against) {
  if (!Meteor.userId()) {
    throw new Meteor.Error("must be logged in for that");
  }
  const game = new SavedHiveGame({
    _id: gameId
  });
  const player = new NetworkHivePlayer(Meteor.userId(), new ManualActionChooser(allowedActions));
  player.connectionId = this.connection.id;
  game.addPlayer(player);

  // TODO: remove or fix
  // Vent.emit(`hiveGame::${gameId}::${player.color}::event`, { type: "init", gameId });
  if (against !== "human") {
    const secondPlayer = new HivePlayer("random", new RandomActionChooser(allowedActions));
    game.addPlayer(secondPlayer);
    Vent.emit(`hiveGame::${gameId}::${player._id}::event`, {
      type: "player", _id: secondPlayer._id, index: 1, color: secondPlayer.color
    });
  }

  SavedHiveGames.insert(game.toJSONValue());
  if (against !== "human") {
    const playerToStartIndex = Math.round(Math.random());
    Vent.emit(`hiveGame::${gameId}::${player._id}::event`, {
      type: "start", playerId: game.players[playerToStartIndex]._id
    });
    game.play(playerToStartIndex);
    if (playerToStartIndex === 1) {
      computersMove(game, game.players[1]);
    }
  }
}

export function joinGame(gameId) {
  const game = SavedHiveGames.findOne({ _id: gameId });
  if (!game) {
    throw new Meteor.Error("Invalid Game ID");
  }
  if (game.players.length === 2) {
    throw new Meteor.Error("Game already has two players");
  }
  if (game.players.find(p => p._id === Meteor.userId())) {
    throw new Meteor.Error("You can't play against yourself!");
  }
  const player = new NetworkHivePlayer(Meteor.userId(), new ManualActionChooser(allowedActions));
  player.connectionId = this.connection.id;
  game.addPlayer(player);
  Vent.emit(`hiveGame::${gameId}::${game.getOpponent(player)._id}::event`, {
    type: "player", _id: player._id, index: 1, color: "#00FF00"
  });
  const playerToStartIndex = Math.round(Math.random());
  Vent.emit(`hiveGame::${gameId}::${game.getOpponent(player)._id}::event`, {
    type: "start", playerId: game.players[playerToStartIndex]._id
  });
  game.play(playerToStartIndex);

  SavedHiveGames.update(
    { _id: gameId },
    { $set: { currentPlayerId: game.players[playerToStartIndex]._id }, $push: { players: game.toJSONValue().players[1] } }
  );
  return game.toJSONValue();
}

export async function makeMove(gameId, pieceId, locationId) {
  const game = SavedHiveGames.findOne({ _id: gameId });
  if (!game) {
    throw new Meteor.Error("Invalid Game ID");
  }
  const player = game.players.find(p => p.connection === this.connection);
  if (!player) {
    throw new Meteor.Error("You aren't playing that game");
  }
  if (player !== game.currentPlayer) {
    throw new Meteor.Error("It isn't your turn");
  }
  takeAction(game, player, pieceId, locationId, false);
  const opponent = game.getOpponent(player);
  SavedHiveGames.update(
    { _id: gameId },
    { $set: { currentPlayerId: opponent._id }, $push: { moves: { playerId: player._id, pieceId, locationId } } }
  );
  if (game.isOver()) {
    const winner = game.winner();
    SavedHiveGames.update(
      { _id: gameId },
      { $set: { winner: { playerId: winner && winner._id, started: game.state.moves[0].playerId === (winner && winner._id) } } }
    );
  }
  Vent.emit(`hiveGame::${gameId}::${opponent._id}::event`, {
    type: "action", playerId: player._id, pieceId, locationId
  });
  if (opponent.actionChooser().name() !== "manual") {
    game.currentPlayer = opponent;
    await computersMove(game, opponent);
  }
}
