export class NetworkChooser {
  constructor(name) {
    this.name = name;
  }

  async chooseAction(player, state) {
    return new Promise((resolve, reject) => {
      Meteor.call("hive.chooseAction", this.name, );
    });
    const actions = this.allowedActions(player, state);
    return new Promise((resolve, reject) => {
      this._awaitingActionResolution = resolve;
    })
    .then((action) => {
      if (!actions.find(a => a.piece === action.piece && a.location === action.location)) {
        throw new Error("invalid action");
      }
      return action;
    });
  }

  name() {
    return "manual";
  }
}
