import HivePlayer from "/api/games_ai/hive/hive-player.js";
import { DDP } from "meteor/ddp";

export default class NetworkHivePlayer extends HivePlayer {
  constructor(...args) {
    super(...args);
    this.connection = DDP._CurrentInvocation.get().connection;
  }
}
