import HiveGame from "/api/games_ai/hive/hive-game.js";
import HivePlayer from "/api/games_ai/hive/hive-player.js";
import ManualActionChooser from "/api/games_ai/common/manual-chooser.js";
import allowedActions from "/api/games_ai/hive/allowedActions.js";
import { Vent } from "meteor/cultofcoders:redis-oplog";
import { Random } from "meteor/random";
import { Tracker } from "meteor/tracker";
import { startGame, joinGame, makeMove } from "./method-stubs.js";

export default class RemoteHiveGame extends HiveGame {
  constructor(gameId, gameType, ...args) {
    super(...args);
    this.isStarter = !gameId;
    this.gameId = gameId || Random.id();
    this.gameType = gameType;
    this.dep = new Tracker.Dependency();
  }

  changed() {
    this.dep.changed();
  }

  depend() {
    this.dep.depend();
  }

  playWhenReady() {
    const handler = Vent.subscribe("hiveGameEvents", this.gameId, Meteor.userId());

    handler.listen((event) => {
      this.parseMessage(event);
    });

    return new Promise((resolve) => {
      Tracker.autorun((comp) => {
        if (handler.ready()) {
          comp.stop();
          resolve();
        }
      });
    })
    .then(() => {
      return new Promise((resolve, reject) => {
        this.state.onAction((state, player, action) => {
          if (player._id !== this.remotePlayerId) {
            makeMove.call({ gameId: this.gameId, pieceId: action.piece._id, locationId: action.location.id() }, (err) => {
              if (err) {
                reject(err);
              }
            });
          }
        });
        if (!this.isStarter) {
          joinGame.call({ gameId: this.gameId }, (err, game) => {
            if (err) {
              reject(err);
              return;
            }
            this.remotePlayerId = game.players[0]._id;
            this.addPlayer(new HivePlayer(game.players[0]._id, new ManualActionChooser(allowedActions)));
            this.addPlayer(new HivePlayer(Meteor.userId(), new ManualActionChooser(allowedActions)));
            this.changed();
            this.play().then(() => {
              resolve();
            })
            .catch((_err) => {
              reject(_err);
            });
          });
        }
        else {
          this.addPlayer(new HivePlayer(Meteor.userId(), new ManualActionChooser(allowedActions)));
          startGame.call({ gameId: this.gameId, against: this.gameType }, (err) => {
            if (err) {
              reject(err);
            }
            else {
              this._resolve = resolve;
              this._reject = reject;
            }
          });
        }
      });
    });
  }

  parseMessage(message) {
    if (message.type === "init") {
      this.gameId = message.gameId;
      console.log(this.gameId);
    }
    else if (message.type === "player") {
      this.addPlayer(new HivePlayer(message._id, new ManualActionChooser(allowedActions)));
      this.remotePlayerId = message._id;
      this.changed();
    }
    else if (message.type === "start") {
      const playerToStart = this.players.find(p => p._id === message.playerId);
      this.play(this.players.indexOf(playerToStart)).then(() => {
        this._resolve();
      })
      .catch((err) => {
        this._reject(err);
      });
    }
    else if (this.remotePlayerId === message.playerId) {
      this.players.find(p => p._id === message.playerId).actionChooser().takeAction({ piece: this.getPiece(message.pieceId), location: this.getLocation(message.locationId) });
    }
  }
}
