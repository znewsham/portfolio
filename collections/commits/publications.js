import { Commits } from "./commits.js";

function commitsPublication() {
  return Commits.find();
}

Meteor.publish("commits", commitsPublication);
