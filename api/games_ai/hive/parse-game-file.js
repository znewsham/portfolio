import "@babel/polyfill";
import { Location } from "./hive-state";
import HiveGame from "./hive-game";
import HivePlayer from "./hive-player";
import allowedActions from "./allowedActions";
import ManualActionTaker from "../common/manual-chooser";

if (global.Meteor) {
  Random = require("meteor/random").Random;
}
else {
  Random = require("meteor-random");
}

export default function parseGame(inputFile, fileContent) {
  const game = new HiveGame();
  const players = [];
  let axialRoot;
  fileContent.split(/\r?\n/).forEach((line) => {
    if ((line.startsWith("P0") || line.startsWith("P1")) && line.includes("[id ")) {
      const actionTaker = new ManualActionTaker(allowedActions);
      let playerId = line.split("\"")[1];
      if (playerId === "guest") {
        playerId = `guest${Random.id()}`;
      }
      const newPlayer = new HivePlayer(playerId, actionTaker);
      game.addPlayer(newPlayer);
      players.push(newPlayer);
    }
    if (line.startsWith(";") && line.includes("[")) {
      const action = line.split(/\[\d+ /)[1].split("]")[0];
      if (action.startsWith("dropb") || action.startsWith("pdropb") || action.startsWith("move") || action.startsWith("movedone")) {
        const playerNumber = parseInt(line.charAt(3), 10);
        const player = players[playerNumber];
        const axial = action.split(" ").slice(-3, -1);
        axial[0] = axial[0].charCodeAt(0) - 65;// capital A = start
        axial[1] = parseInt(axial[1], 10);
        let pieceId;
        if (action.startsWith("pdropb")) {
          pieceId = action.split(" ")[action.startsWith("move") ? 2 : 1].replace("g", "h").replace("G", "H").substr(1);
        }
        else {
          pieceId = action.split(" ")[action.startsWith("move") ? 2 : 1].replace("G", "H").replace("g", "h").match(/[A-Za-z]\d?$/)[0];
        }
        const piece = game.getPiece(`${player._id}-${pieceId.toLowerCase()}`);
        const locationName = action.split(" ").slice(-1)[0];
        let locationId;
        if (locationName === "." && game.state.moves.length === 0) {
          locationId = "0,0,0";
          axialRoot = axial;
        }
        else {
          const actualAxial = axial.map((a, i) => a - axialRoot[i]);
          const cube = Location.AxialToCube({ q: actualAxial[0], r: -actualAxial[1] });
          locationId = `${cube.x},${cube.y},${cube.z}`;
        }
        const location = game.getLocation(locationId);
        if (piece.class !== "beatle" && location.pieces.length) {
          console.log(piece._id, location.pieces.map(p => p._id));
          //throw new Error("bad piece");
        }
        game.state.action(player, { piece, location });
      }
    }
  });
  const winner = game.isOver() && game.winner();
  if (game.state.moves.length >= 2) {
    const moves = game.state.moves.map((m) => {
      const movePlayer = players.find(p => p._id === m.piece._id.split("-")[0]);
      return `${m.piece._id.replace(`${movePlayer._id}-`, players.indexOf(movePlayer))}${m.location.id().split(",").map((i) => {
        const neg = i < 0;
        if (neg) {
          return "n" + `${0 - i}`.padStart(2, "0");
        }
        return `${i}`.padStart(2, "0");
      }).join("")}`;
    }).join(" ");
    return { game, players, winner, moves };
  }
}
