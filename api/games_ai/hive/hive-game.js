import _ from "underscore";
import Game from "../common/game.js";
import HiveState from "./hive-state.js";
import { Queen } from "./tiles.js";

export default class HiveGame extends Game {
  static Colors = [
    "#EBDB97",
    "#383933"
  ];

  constructor() {
    super(new HiveState());
    this.promise = new Promise((resolve) => {
      this.__resolve = resolve;
    });
  }

  detectCycles(n) {
    if (this.state.moves.length <= n * 2) {
      return false;
    }
    const usefulMoves = this.state.moves.slice(-(n * 2));
    for (let i = 0; i < n; i++) {
      if (usefulMoves[i].piece === usefulMoves[i + n].piece && usefulMoves[i].location === usefulMoves[i + n].location) {
        return true;
      }
    }
  }

  isOver() {
    const queen1 = this.player1.pieces.find(p => p instanceof Queen);
    const queen2 = this.player2.pieces.find(p => p instanceof Queen);
    const won = (queen1.location && queen1.location.neighbours.filter(n => n.pieces.length).length === 6) || (queen2.location && queen2.location.neighbours.filter(n => n.pieces.length).length === 6);
    if (won) {
      return true;
    }
    if (this.detectCycles(2)) {
      return true;
    }
  }

  winner() {
    return this.player1.score(this) > this.player2.score(this) ? this.player1 : this.player2;
  }

  getPiece(pieceId) {
    return _.flatten(this.players.map(p => p.pieces)).find(p => p._id === pieceId);
  }

  getLocation(locationId) {
    return this.state.locations.get(locationId);
  }

  waitForFinish() {
    return this.promise;
  }

  addPlayer(player) {
    if (!player.color) {
      player.color = HiveGame.Colors[this.players.length];
    }
    return super.addPlayer(player);
  }

  async play(current = 0) {
    while (true) {
      const player = this.players[current];
      this.currentPlayer = player;
      try {
        const action = await player.chooseAction(this.state, this.players[(current + 1) % 2].pieces);
        if (action) {
          this.state.action(player, action);
        }
        current = (current + 1) % 2;
      }
      catch (e) {
        console.log(e);
      }
      if (this.isOver()) {
        break;
      }
    }
    return this.winner();
  }
}
