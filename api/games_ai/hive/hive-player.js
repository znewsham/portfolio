import Player from "../common/player.js";
import { Hopper, Queen, Ant, Beatle, Spider } from "./tiles.js";

export default class HivePlayer extends Player{
  constructor(_id, ...args) {
    super(...args);
    this._id = _id;
    this.pieces = [
      new Hopper(_id, "h1"),
      new Hopper(_id, "h2"),
      new Hopper(_id, "h3"),
      new Ant(_id, "a1"),
      new Ant(_id, "a2"),
      new Ant(_id, "a3"),
      new Spider(_id, "s1"),
      new Spider(_id, "s2"),
      new Beatle(_id, "b1"),
      new Beatle(_id, "b2"),
      new Queen(_id, "q")
    ];
  }

  // NOTE: if the player won, they get a score which gives 1 point for each empty space surrounding their queen, and an additional maximum of 5 points based on the number of moves (fewer moves is better)
  score(game) {
    const otherPlayer = game.players.find(p => p !== this);
    const otherQueen = otherPlayer.pieces.find(p => p instanceof Queen);
    const myQueen = this.pieces.find(p => p instanceof Queen);

    const otherQueenSpaces = otherQueen.location.neighbours.filter(l => l.pieces.length === 0).length;
    const myQueenSpaces = myQueen.location.neighbours.filter(l => l.pieces.length === 0).length;

    if (otherQueenSpaces !== 0 && myQueenSpaces !== 0) {
      return 0;
    }
    if (otherQueenSpaces === 0 && myQueenSpaces !== 0) {
      return myQueenSpaces + Math.min(Math.max(5, 5 / 90 * (100 - game.state.moves.length)), 0);
    }
    else if (otherQueenSpaces === 0 && myQueenSpaces === 0) {
      return 0;
    }
    else {
      let baseScore = -10;
      baseScore = baseScore + (5 - otherQueenSpaces);
      return baseScore + Math.max(4, Math.min(0, (game.state.moves.length - 100) / 10 ));
    }
  }
}
