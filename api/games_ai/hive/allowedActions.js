import _ from "underscore";


export default function allowedActions(player, state) {
  return _.flatten(player.pieces.map(piece => piece.allowedMoves(state).map(location => ({ piece, location }))));
}
