import { Tile } from "../tiles.mjs";
import { Location } from "../hive-state.mjs";

const tile1 = new Tile();
const tile2 = new Tile();
const tile3 = new Tile();
const tile4 = new Tile();
const tile5 = new Tile();

tile1.location = new Location(0, 0, 0);
tile2.location = new Location(0, -1, 1);
tile3.location = new Location(-1, 1, 0);
tile4.location = new Location(-1, 1, 0);
tile5.location = new Location(-1, 1, 0);

/*

  1   2   3
    4   5
*/
tile1.location.neighbours.push(tile2.location);
tile2.location.neighbours.push(tile1.location);
tile1.location.neighbours.push(tile4.location);
tile4.location.neighbours.push(tile1.location);
tile4.location.neighbours.push(tile5.location);
tile5.location.neighbours.push(tile4.location);
tile3.location.neighbours.push(tile2.location);
tile2.location.neighbours.push(tile3.location);
tile5.location.neighbours.push(tile2.location);
tile2.location.neighbours.push(tile5.location);
tile4.location.neighbours.push(tile2.location);
tile2.location.neighbours.push(tile4.location);
tile5.location.neighbours.push(tile3.location);
tile3.location.neighbours.push(tile5.location);

tile1.location.pieces.push(tile1);
tile2.location.pieces.push(tile2);
tile3.location.pieces.push(tile3);
tile4.location.pieces.push(tile4);
tile5.location.pieces.push(tile5);

if (tile2.isPinned()) {
  console.log("pinned");
}
else {
  console.log("not pinned");
}
