import HiveState from "../hive-state.mjs";
import { Queen, Beatle, Hopper } from "../tiles.mjs";

const state = new HiveState();

const queen = new Queen(1);

state.move(queen, state.getLocation(0, 0, 0));

const beatle = new Beatle(2);

console.log(beatle.allowedMoves(state));

/*

let allowedLocations = beatle.allowedMoves(state);

state.move(beatle, allowedLocations[0]);

allowedLocations = beatle.allowedMoves(state);

const hopper = new Hopper(1);

state.move(hopper, allowedLocations[1]);

console.log(hopper.allowedMoves(state));

*/
