import HiveState from "../hive-state.mjs";
import { Queen, Spider } from "../tiles.mjs";

const state = new HiveState();

const queen = new Queen(1);

state.move(queen, state.getLocation(0, 0, 0));
const queen2 = new Queen(2);
let allowedLocations = queen2.allowedMoves(state);
state.move(queen2, allowedLocations[0]);

const spider = new Spider(2);
allowedLocations = spider.allowedMoves(state);


state.move(spider, allowedLocations[0]);
allowedLocations = spider.allowedMoves(state);
console.log(allowedLocations.length);
