import _ from "underscore";
import { Location } from "./hive-state.js";

export class Tile {
  constructor(playerId, pieceId) {
    this.playerId = playerId;
    this.location = null;
    this._id = `${this.playerId}-${pieceId}`;
  }

  isPinned(state) {
    if (!this.location) {
      return false;
    }
    const occupiedNeighbours = this.location.neighbours.filter(n => n.pieces.length);
    if (occupiedNeighbours.length === 0) {
      throw new Error("Not possible - no neighbours");
    }
    if (occupiedNeighbours.length === 1) {
      return false;
    }
    if (this.location.pieces.length > 1) {
      return false;
    }
    let someNeighbour = occupiedNeighbours[0];
    return occupiedNeighbours.some((neighbour) => {
      return !neighbour.isConnectedTo(someNeighbour, [this.location]);
    });
  }

  allowedMoves(state) {
    if (!state.placedPieces.find(p => (p instanceof Queen) && p.playerId === this.playerId) && !(this instanceof Queen)) {
      if (state.moves.filter(m => m.piece.playerId === this.playerId).length === 3) {
        return [];
      }
    }
    if (!this.location) {
      if (state.placedPieces.length === 0) {
        return [state.locations.get("0,0,0")];
      }
      return _.unique(_.flatten(state.placedPieces.map(piece => {
        return piece.location.neighbours.filter((n) => {
          if (n.pieces.length !== 0) {
            return false;
          }
          if (state.placedPieces.length === 1) {
            return true;
          }
          return !n.neighbours.find(n1 => n1.pieces.length && n1.pieces.slice(-1)[0].playerId !== this.playerId);
        });
      })));
    }
    else if (this.isPinned(state)) {
      throw new Error("piece pinned");
    }
  }
}

Tile.id = 0;

export class SlidingTile extends Tile {
  constructor(playerId, pieceId, numPlaces) {
    super(playerId, pieceId);
    this.numPlaces = numPlaces;
  }

  allowedMoves(state) {
    try {
      const allowed = super.allowedMoves(state);
      if (allowed) {
        return allowed;
      }
    }
    catch (e) {
      return [];
    }

    return this.location.slidePath(this.numPlaces, [this.location]);
  }
}

export class Hopper extends Tile {
  constructor(...args) {
    super(...args);
    this.class = "hopper";
  }

  allowedMoves(state) {
    try {
      const allowed = super.allowedMoves(state);
      if (allowed) {
        return allowed;
      }
    }
    catch (e) {
      return [];
    }

    const starting = this.location.neighbours.filter(n => n.pieces.length);
    return starting.map((startingLocation) => {
      const direction = this.location.getDirectionOf(startingLocation);
      const addressOffset = Location.AddressOfNeighbour(direction);
      let finalLocation = startingLocation;
      while(finalLocation.pieces.length) {
        finalLocation = state.getLocation(finalLocation.x + addressOffset.x, finalLocation.y + addressOffset.y, finalLocation.z + addressOffset.z);
        if (!finalLocation) {
          console.log(addressOffset);
        }
      }
      return finalLocation;
    });
  }
}
Hopper.Count = 3;

export class Beatle extends Tile {
  constructor(...args) {
    super(...args);
    this.class = "beatle";
  }

  allowedMoves(state) {
    try {
      const allowed = super.allowedMoves(state);
      if (allowed) {
        return allowed;
      }
    }
    catch (e) {
      return [];
    }

    const withPieces = this.location.neighbours.filter(n => n.pieces.length);
    if (this.location.pieces.length > 1) {
      withPieces.push(this.location);
    }
    const isAdjacent = _.flatten(withPieces.map(n => n.neighbours)).filter(n => n.isAdjacentTo(this.location));

    return _.union(withPieces, isAdjacent);
  }
}
Beatle.Count = 2;

export class Queen extends SlidingTile {
  constructor(playerId, pieceId) {
    super(playerId, pieceId, 1);
    this.class = "queen";
  }
}
Queen.Count = 1;

export class Ant extends SlidingTile {
  constructor(playerId, pieceId) {
    super(playerId, pieceId, false);
    this.class = "ant";
  }
}
Ant.Count = 3;

export class Spider extends SlidingTile {
  constructor(playerId, pieceId) {
    super(playerId, pieceId, 3);
    this.class = "spider";
  }
}
Spider.Count = 2;
