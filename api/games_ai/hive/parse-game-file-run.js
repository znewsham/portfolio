import fs from "fs";
import parseGame from "./parse-game-file.js";

const inputFile = process.argv[2];
const fileContent = fs.readFileSync(inputFile).toString();

try {
  const { players, winner, game, moves } = parseGame(inputFile, fileContent);
  console.log(`${players[0]._id},${players[1]._id},${winner && players[0].score(game)},${winner && players[1].score(game)},${winner && winner._id},${players.indexOf(winner)},"${moves}"`);
}
catch (e) {
  console.error(e);
  console.error(process.argv[2]);
}
