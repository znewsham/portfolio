import _ from "underscore";
import { Queen } from "./tiles.js";

export class Location {
  constructor(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.neighbours = [];
    this.pieces = [];
  }

  id() {
    return `${this.x},${this.y},${this.z}`;
  }

  getDirectionOf(location) {
    for (let i = 0; i < 6; i++) {
      const addressOffset = Location.AddressOfNeighbour(i);
      if (location.x === this.x + addressOffset.x && location.y === this.y + addressOffset.y && location.z === this.z + addressOffset.z) {
        return i;
      }
    }
    throw new Error("Not a neighbour");
  }

  isConnectedTo(location, locationsToIgnore = []) {
    if (location === this) {
      return true;
    }
    const neighbour = this.neighbours.find(n => n === location);
    if (neighbour) {
      return true;
    }
    return this.neighbours
    .filter(n => n.pieces.length && !locationsToIgnore.includes(n))
    .some(n => n.isConnectedTo(location, [this, ...locationsToIgnore]));
  }

  isAdjacentTo(location) {
    return this.neighbours.includes(location);
  }

  slidePath(numPlaces, toIgnore = []) {
    const withPieces = this.neighbours.filter(n => n.pieces.length).filter(n => n !== toIgnore[0]);
    const slidable = this.neighbours
    .filter(n => !toIgnore.includes(n))
    .filter(n => this.canSlideInto(n, toIgnore[0]))
    .filter(n => _.intersection(n.neighbours, withPieces).length);
    if (numPlaces === 1) {
      return slidable;
    }
    else if (numPlaces === false) {
      return _.unique(_.union(slidable, _.flatten(slidable.map(s => s.slidePath(false, [...toIgnore, s])))));
    }
    return _.flatten(slidable.map(s => s.slidePath(numPlaces - 1, [...toIgnore, s])));
  }

  canSlideInto(location, movingPiece) {
    if (location.pieces.length !== 0) {
      return false;
    }
    let direction = this.getDirectionOf(location);
    const n1 = this.neighbourAtDirection(direction - 1);
    const n2 = this.neighbourAtDirection(direction + 1);
    return (!n1 || n1.pieces.length === 0 || n1 === movingPiece) || (!n2 || n2.pieces.length === 0 || n2 === movingPiece);
  }

  neighbourAtDirection(direction) {
    if (direction < 0) {
      direction = 6 + direction;
    }
    const offset = Location.AddressOfNeighbour(direction % 6);
    return this.neighbours.find(n => n.x === this.x + offset.x && n.y === this.y + offset.y && n.z === this.z + offset.z);
  }

  static AxialToCube({ q, r }) {
    const x = q;
    const z = r;
    const y = -x - z;
    return { x, y, z };
  }

  static AddressOfNeighbour(direction) {
    const directions = [
      {x: +1, y:  -1, z:  0}, {x: +1, y:  0, z:  -1}, {x: 0, y:  +1, z:  -1},
      {x: -1, y:  +1, z:  0}, {x: -1, y:  0, z:  +1}, {x: 0, y:  -1, z:  +1},
    ];
    return directions[direction];
  }

}

export default class HiveState {
  constructor() {
    this.locations = new Map();
    this.placedPieces = [];
    this.queensPlaced = 0;
    this.moves = [];
    this.afterActionHandlers = [];
    const center = new Location(0, 0, 0);
    this.locations.set(`0,0,0`, center);
    for (let x = -20; x <= 20; x++) {
      for (let y = Math.max(-20, -x-20); y <= Math.min(20, -x+20); y ++) {
        const z = -x-y;
        this.locations.set(`${x},${y},${z}`, new Location(x, y, z));
      }
    }
    for (let [key, location] of this.locations.entries()) {
      for (let i = 0; i < 6; i++) {
        const neighbourOffset = Location.AddressOfNeighbour(i);
        location.neighbours.push(this.locations.get(`${location.x+neighbourOffset.x},${location.y+neighbourOffset.y},${location.z+neighbourOffset.z}`));
      }
      location.neighbours = _.compact(location.neighbours);
    }
  }

  deal() {

  }

  getLocation(x, y, z) {
    if (y === undefined && z === undefined) {
      return this.locations.get(x);
    }
    return this.locations.get(`${x},${y},${z}`);
  }

  move(tile, location) {
    if (tile instanceof Queen && !tile.location) {
      this.queensPlaced++;
    }
    if (!tile.location) {
      this.placedPieces.push(tile);
    }
    else {
      const index = tile.location.pieces.indexOf(tile);
      if (index >= 0) {
        tile.location.pieces.splice(index, 1);
      }
    }
    tile.location = location;
    location.pieces.push(tile);
    this.moves.push({ piece: tile, location });
  }

  action(player, action) {
    const oldLocation = action.piece.location;
    this.move(action.piece, action.location);
    this.afterActionHandlers.forEach((handler) => {
      handler(this, player, { oldLocation, ...action });
    });
  }

  onAction(handler) {
    this.afterActionHandlers.push(handler);
  }
}
