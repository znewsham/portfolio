import _ from "underscore";

export default class Game {
  constructor(state) {
    this.state = state;
    this.players = [];
  }

  addPlayer(player) {
    if (this.player2) {
      throw new Error("Has two players");
    }
    if (!this.player1) {
      this.player1 = player;
    }
    else if (!this.player2) {
      this.player2 = player;
    }
    this.players.push(player);
  }

  getOpponent(player) {
    return this.players[0] === player ? this.players[1] : this.players[0];
  }

  winner() {
    throw new Error("Abstract method winner called");
  }

  play() {
    throw new Error("Abstract method play called");
  }
}
