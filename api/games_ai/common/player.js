export default class Player {
  constructor(actionChooser) {
    this._actionChooser = actionChooser;
    this.id = Player.id++;
  }

  score() {
    throw new Error("Abstract method score called");
  }

  chooseAction(state, opponentPublic) {
    return this._actionChooser.chooseAction(this, state, opponentPublic);
  }

  actionChooser() {
    return this._actionChooser;
  }
}
