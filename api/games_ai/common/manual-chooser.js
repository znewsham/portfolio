import _ from "underscore";

export default class ManualActionTaker {
  constructor(allowedActions) {
    this.allowedActions = allowedActions;
    this._awaitingActionResolution;
  }

  takeAction(action) {
    this._awaitingActionResolution(action);
  }

  async chooseAction(player, state) {
    const actions = this.allowedActions(player, state);
    return new Promise((resolve, reject) => {
      this._awaitingActionResolution = resolve;
    })
    .then((action) => {
      if (!actions.find(a => a.piece === action.piece && a.location === action.location)) {
        throw new Error("invalid action");
      }
      return action;
    });
  }

  name() {
    return "manual";
  }
}
