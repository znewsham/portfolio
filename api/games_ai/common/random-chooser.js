import _ from "underscore";

export default class RandomActionTaker {
  constructor(allowedActions, delay) {
    this.allowedActions = allowedActions;
    this.delay = delay;
  }

  chooseAction(player, state) {
    const actions = this.allowedActions(player, state);
    const action = _.shuffle(actions)[0];
    if (this.delay) {
      return new Promise((resolve) => {
        setTimeout(() => resolve(action), this.delay);
      });
    }
    return action;
  }

  name() {
    return "random";
  }
}
