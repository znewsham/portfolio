import Game from "../common/game.mjs";
import State from "./state.mjs";

export default class JaipurGame extends Game {
  constructor(player1, player2) {
    super(player1, player2, new State());
  }

  winner() {
    return this.player1.score() > this.player2.score() ? this.player1 : this.player2;
  }

  play() {
    let current = 0;
    let lastAction;
    while (true) {
      const player = this.players[current];
      const action = player.chooseAction(this.state, this.state.playerPublic[this.players[(current + 1) % 2].id]);
      if (!action) {
        break;
      }
      lastAction = action;
      this.state.action(player, action);
      current = (current + 1) % 2;
    }

    if (this.player1.herd.length > this.player2.herd.length) {
      this.player1.tokens.push(this.state.camelToken);
    }

    if (this.player2.herd.length > this.player1.herd.length) {
      this.player2.tokens.push(this.state.camelToken);
    }

    return this.winner();
  }
}
