export default class Card {
  constructor(suit, cardId, value) {
    this.cardId = cardId;
    this.suit = suit;
    this.value = value;
  }
}
