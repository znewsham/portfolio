import allowedActions from "./allowedActions.mjs";
import _ from "underscore";

export default class ZackChooser {
  chooseAction(player, state, otherPlayerPublic) {
    const actions = allowedActions(player, state);
    const action = _.shuffle(actions)[0];
    return action;
  }

  name() {
    return "zack";
  }
}
