import Player from "../common/player.mjs";

export default class JaipurPlayer {
  score() {
    return this.tokens.reduce((score, token) => token.value + score, 0);
  }
}
Player.id = 1;
