import SUITS from "./suits.mjs";
import _ from "underscore";
import TOKEN_TYPES from "./tokenTypes.mjs";
import ACTIONS from "./actions.mjs";

const min2Suits = [
  SUITS.SILVER,
  SUITS.GOLD,
  SUITS.DIAMOND
];




export class Action {
  constructor(action, cards) {
    this.action = action;
    this.cards = cards;
  }
}

export function allowedSellActions(player, state) {
  const actions = [];
  const groupedHand = _.groupBy(player.hand, "suit");
  _.each(groupedHand, (cards, suit) => {
    let cardsRequired = min2Suits.includes(suit) ? 2 : 1;
    let i = cards.length;
    while (i >= cardsRequired) {
      actions.push(new Action(ACTIONS.SELL, cards.slice(0, i)));
      i --;
    }
  });
  return actions;
}

export function allowedTakeActions(player, state) {
  if (player.hand.length >= 7) {
    return [];
  }
  return state.market.filter(card => card.suit !== SUITS.CAMEL).map(card => new Action(ACTIONS.TAKE, [card]));
}

function allowedCamelActions(player, state) {
  const camels =  state.market.filter(card => card.suit === SUITS.CAMEL);
  if (camels.length) {
    return [
      new Action(ACTIONS.CAMEL, camels)
    ];
  }
  return [];
}

function getCombinations(chars) {
  var result = [];
  var f = function(prefix, chars) {
    for (var i = 0; i < chars.length; i++) {
      const join = _.union(prefix, [chars[i]]);
      result.push(join);
      f(join, chars.slice(i + 1));
    }
  }
  f([], chars);
  return result;
}

function allowedExchangeActions(player, state) {
  let nonCamelCards = state.market.filter(card => card.suit !== SUITS.CAMEL);
  if (nonCamelCards.length < 2) {
    return [];
  }

  let marketCombinations = getCombinations(nonCamelCards).filter(c => c.length > 1);
  let playerCombinations = getCombinations(_.union(player.herd, player.hand));
  const actions = [];
  marketCombinations.forEach(marketCombo => {
    const validCombos = playerCombinations.filter(playerCombo => {
      return playerCombo.length === marketCombo.length && player.hand.length - playerCombo.filter(c => c.suit !== SUITS.CAMEL) + marketCombo.length <= 7;
    });

    actions.push(...validCombos.map(playerCombo => {
      let action = new Action(ACTIONS.EXCHANGE, _.union(marketCombo, playerCombo));
      action.marketLength = marketCombo.length;
      action.playerlength = playerCombo.length;
      return action;
    }));
  });
  return actions;
}

export default function allowedActions(player, state) {
  if (state.market.length !== 5) {
    return [];
  }
  if (_.values(state.goodsTokens).filter(gt => gt.length === 0).length === 3) {
    return [];
  }
  return _.union(
    allowedSellActions(player, state),
    allowedTakeActions(player, state),
    allowedCamelActions(player, state),
    allowedExchangeActions(player, state)
  );
}
