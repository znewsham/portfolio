export default {
  TAKE: "TAKE",
  EXCHANGE: "EXCHANGE",
  CAMEL: "CAMEL",
  SELL: "SELL"
};
