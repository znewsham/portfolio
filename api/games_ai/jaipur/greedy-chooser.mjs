import allowedActions, { allowedTakeActions, allowedSellActions } from "./allowedActions.mjs";
import _ from "underscore";

/**
  1) exchange the maximum number of lowest value hand cards (including camel) for the highest value cards available
**/
export default class GreedyChooser {
  chooseAction(player, state, otherPlayerPublic) {
    const takeActions = allowedTakeActions(player, state);
    if (takeActions.length) {
      const sorted = _.sortBy(takeActions, ta => {
        const card = ta.cards[0];
        const token = state.goodsTokens[card.suit].slice(-1)[0]
        return token ? token.value : 0;
      });
      return sorted[sorted.length - 1];
    }
    const sellActions = allowedSellActions(player, state);
    if (sellActions.length) {
      const sorted = _.sortBy(sellActions, ta => {
        const goodsIndexes = {};
        return ta.cards.reduce((memo, card) => {
          if (!goodsIndexes[card.suit]) {
            goodsIndexes[card.suit] = 1;
          }
          const tokens = state.goodsTokens[card.suit];
          const token = tokens[tokens.length - goodsIndexes[card.suit]++]
          return token ? token.value : 0;
        }, 0);
      });
      return sorted[sorted.length - 1];
    }
    const actions = allowedActions(player, state);
    const action = _.shuffle(actions)[0];
    return action;
  }

  name() {
    return "greedy";
  }
}
