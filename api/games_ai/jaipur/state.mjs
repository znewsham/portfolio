import Card from "./card.mjs";
import SUITS from "./suits.mjs";
import _ from "underscore";
import TOKEN_TYPES from "./tokenTypes.mjs";
import Token from "./token.mjs";
import ACTIONS from "./actions.mjs";

export default class State {
  constructor() {
    this.cardId = 1;
    this.actions = 0;
    this.player1Public = {
      cardCount: 0,
      knownCards: [],
      herdCount: 0
    };
    this.player2Public = {
      cardCount: 0,
      knownCards: [],
      herdCount: 0
    };
    this.playerPublic = {};
    this.play
    this.camelToken = new Token(TOKEN_TYPES.CAMEL, 5);
    this.market = [
      new Card(SUITS.CAMEL, this.cardId++, 1),
      new Card(SUITS.CAMEL, this.cardId++, 1),
      new Card(SUITS.CAMEL, this.cardId++, 1)
    ];

    this.goodsTokens = {};
    this.bonusTokens = {
      3: [],
      4: [],
      5: []
    };

    this.deck = [];
    this.discard = [];
    this.buildDeck();
    this.setupTokens();
  }

  setupTokens() {
    this.goodsTokens[TOKEN_TYPES.LEATHER] = [];
    for (let i = 1; i <= 11; i++) {
      if (i <= 8) {
        this.goodsTokens[TOKEN_TYPES.LEATHER].push(new Token(TOKEN_TYPES.LEATHER, 1));
      }
      else {
        this.goodsTokens[TOKEN_TYPES.LEATHER].push(new Token(TOKEN_TYPES.LEATHER, 4 - (11 - i)));
      }
    }

    this.goodsTokens[TOKEN_TYPES.CLOTH] = [];
    this.goodsTokens[TOKEN_TYPES.SPICE] = [];
    for (let i = 1; i <= 7; i++) {
      if (i <= 2) {
        this.goodsTokens[TOKEN_TYPES.CLOTH].push(new Token(TOKEN_TYPES.CLOTH, 1));
        this.goodsTokens[TOKEN_TYPES.SPICE].push(new Token(TOKEN_TYPES.SPICE, 1));
      }
      else if (i <= 4){
        this.goodsTokens[TOKEN_TYPES.CLOTH].push(new Token(TOKEN_TYPES.CLOTH, 2));
        this.goodsTokens[TOKEN_TYPES.SPICE].push(new Token(TOKEN_TYPES.SPICE, 2));
      }
      else if (i <= 6){
        this.goodsTokens[TOKEN_TYPES.CLOTH].push(new Token(TOKEN_TYPES.CLOTH, 3));
        this.goodsTokens[TOKEN_TYPES.SPICE].push(new Token(TOKEN_TYPES.SPICE, 3));
      }
      else {
        this.goodsTokens[TOKEN_TYPES.CLOTH].push(new Token(TOKEN_TYPES.CLOTH, 5));
          this.goodsTokens[TOKEN_TYPES.SPICE].push(new Token(TOKEN_TYPES.SPICE, 5));
      }
    }

    this.goodsTokens[TOKEN_TYPES.SILVER] = [];
    for (let i = 1; i <= 5; i++) {
      this.goodsTokens[TOKEN_TYPES.SILVER].push(new Token(TOKEN_TYPES.SILVER, 5));
    }
    this.goodsTokens[TOKEN_TYPES.GOLD] = [];
    for (let i = 1; i <= 5; i++) {
      if (i <= 3) {
        this.goodsTokens[TOKEN_TYPES.GOLD].push(new Token(TOKEN_TYPES.GOLD, 5));
      }
      else {
        this.goodsTokens[TOKEN_TYPES.GOLD].push(new Token(TOKEN_TYPES.GOLD, 6));
      }
    }

    this.goodsTokens[TOKEN_TYPES.DIAMOND] = [];
    for (let i = 1; i <= 5; i++) {
      if (i <= 3) {
        this.goodsTokens[TOKEN_TYPES.DIAMOND].push(new Token(TOKEN_TYPES.DIAMOND, 5));
      }
      else {
        this.goodsTokens[TOKEN_TYPES.DIAMOND].push(new Token(TOKEN_TYPES.DIAMOND, 7));
      }
    }

    for (let i = 1; i <= 7; i++) {
      const rand = Math.random();
      if (rand === 0) {
        i--;
        continue;
      }
      this.bonusTokens[3].push(new Token(TOKEN_TYPES.BONUS, Math.ceil(rand * 3)));
    }

    for (let i = 1; i <= 6; i++) {
      const rand = Math.random();
      if (rand === 0) {
        i--;
        continue;
      }
      this.bonusTokens[4].push(new Token(TOKEN_TYPES.BONUS, Math.ceil(rand * 4)));
    }

    for (let i = 1; i <= 5; i++) {
      const rand = Math.random();
      if (rand === 0) {
        i--;
        continue;
      }
      this.bonusTokens[5].push(new Token(TOKEN_TYPES.BONUS, Math.ceil(rand * 5)));
    }
  }

  buildDeck() {
    for (let i = 0; i < 6; i++) {
      this.deck.push(new Card(SUITS.DIAMOND, this.cardId++, 1));
      this.deck.push(new Card(SUITS.GOLD, this.cardId++, 1));
      this.deck.push(new Card(SUITS.SILVER, this.cardId++, 1));
    }
    for (let i = 0; i < 8; i++) {
      this.deck.push(new Card(SUITS.CLOTH, this.cardId++, 1));
      this.deck.push(new Card(SUITS.SPICE, this.cardId++, 1));
    }
    for (let i = 0; i < 10; i++) {
      this.deck.push(new Card(SUITS.LEATHER, this.cardId++, 1));
    }
    for (let i = 0; i < 8; i++) {
      this.deck.push(new Card(SUITS.CAMEL, this.cardId++, 1));
    }
    this.deck = _.shuffle(this.deck);
  }

  deal(player1, player2) {
    this.playerPublic[player1.id] = this.player1Public;
    this.playerPublic[player2.id] = this.player2Public;
    this.market.push(this.deck.pop());
    this.market.push(this.deck.pop());

    for(let i = 0; i < 5; i++) {
      let card = this.deck.pop();
      if (card.suit === SUITS.CAMEL) {
        this.player1Public.herdCount++;
        player1.herd.push(card);
      }
      else {
        this.player2Public.cardCount++;
        player1.hand.push(card);
      }
      card = this.deck.pop();
      if (card.suit === SUITS.CAMEL) {
        this.player2Public.herdCount++;
        player2.herd.push(card);
      }
      else {
        this.player2Public.cardCount++;
        player2.hand.push(card);
      }
    }
  }

  action(player, action) {
    this.actions++;
    if (action.action === ACTIONS.SELL) {
      this._sellAction(player, action);
    }
    else if (action.action === ACTIONS.CAMEL) {
      this._camelAction(player, action);
    }
    else if (action.action === ACTIONS.TAKE) {
      this._takeAction(player, action);
    }
    else if (action.action === ACTIONS.EXCHANGE) {
      this._exchangeAction(player, action);
    }
  }

  _exchangeAction(player, action) {
    action.cards.forEach(card => {
      if (this.market.indexOf(card) === -1) {
        this.market.push(card);
        if (player.hand.indexOf(card) !== -1) {
          this.playerPublic[player.id].knownCards.splice(this.playerPublic[player.id].knownCards.indexOf(card), 1);
          player.hand.splice(player.hand.indexOf(card), 1);
          this.playerPublic[player.id].cardCount--;
        }
        else if (player.herd.indexOf(card) !== -1) {
          player.herd.splice(player.herd.indexOf(card), 1);
          this.playerPublic[player.id].herdCount--;
        }
      }
      else {
        player.hand.push(card);
        this.playerPublic[player.id].knownCards.push(card);
        this.playerPublic[player.id].cardCount++;
        this.market.splice(this.market.indexOf(card), 1);
      }
    });
  }

  _takeAction(player, action) {
    player.hand.push(...action.cards);
    if (action.cards.length !== 1) {
      throw new Error("bad take");
    }
    this.playerPublic[player.id].cardCount += action.cards.length;
    this.playerPublic[player.id].knownCards.push(...action.cards);
    action.cards.forEach(card => {
      this.market.splice(this.market.indexOf(card), 1);
    });
    while(this.market.length < 5 && this.deck.length) {
      this.market.push(this.deck.pop());
    }
  }

  _camelAction(player, action) {
    player.herd.push(...action.cards);
    this.playerPublic[player.id].herdCount += action.cards.length;
    action.cards.forEach(card => {
      this.market.splice(this.market.indexOf(card), 1);
    });
    while(this.market.length < 5 && this.deck.length) {
      this.market.push(this.deck.pop());
    }
  }

  _sellAction(player, action) {
    const tokens = [];
    action.cards.forEach(card => {
      tokens.push(this.goodsTokens[card.suit].pop());
      player.hand.splice(player.hand.indexOf(card), 1);
      this.playerPublic[player.id].knownCards.splice(this.playerPublic[player.id].knownCards.indexOf(card), 1);
      this.playerPublic[player.id].cardCount --;
    });
    const numTokens = tokens.length > 5 ? 5 : tokens.length;
    if (this.bonusTokens[numTokens] && this.bonusTokens[numTokens].length) {
      tokens.push(this.bonusTokens[numTokens].pop());
    }
    this.discard.push(...action.cards);
    player.tokens.push(..._.compact(tokens));
  }
}
