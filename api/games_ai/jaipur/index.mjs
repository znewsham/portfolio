import Game from "./game.mjs";
import Player from "./player.mjs";
import RandomChooser from "./random-chooser.mjs";
import ZackChooser from "./zack-chooser.mjs";
import GreedyChooser from "./greedy-chooser.mjs";
const score = {

};
for (let i = 0; i < parseInt(process.argv[2]); i++) {
  const player1 = new Player(new RandomChooser());
  const player2 = new Player(new GreedyChooser());
  let player1First = Math.random() >= 0.5 ? player1 : player2;
  const game = new Game(player1First ? player1 : player2, player1First ? player2 : player1);
  const winner = game.play();
  score[winner.actionChooser.name()] = score[winner.actionChooser.name()] ? score[winner.actionChooser.name()] + 1 : 1;
}

console.log(score);
