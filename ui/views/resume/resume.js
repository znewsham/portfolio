import _ from "underscore";
import moment from "moment";
import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
import { Commits } from "/collections/commits/commits.js";
import { Graph } from "./commit-graph.js";
import select2Init from "/vendors/select2.full.min.js";
import "/ui/components/modal/modal.js";
import "./segments/justplay.html";
import "./segments/mobiplus.html";
import "./segments/mushroomcloud.html";
import "./segments/qnx.html";
import "./segments/vworker.html";
import "./segments/bt.html";
import "./segments/eagleeye.html";
import "./segments/irdeto.html";
import "./resume.html";


if (Meteor.isClient) {
  select2Init(window, $);
}
function isElementInViewport(el) {
  if (typeof jQuery === "function" && el instanceof jQuery) {
    el = el[0];
  }

  const rect = el.getBoundingClientRect();

  return (
    rect.top >= 0
    && rect.left >= 0
    && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) /* or $(window).height() */
    && rect.right <= (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */
  );
}

export class ResumeView extends BlazeComponent {
  static HelperMap() {
    return [
      "resumeElements",
      "graph",
      "branches",
      "dateRange",
      "templateName",
      "selectedCommit",
      "modalTitle",
      "cameFromInfoIcon"
    ];
  }

  static EventMap() {
    return {
      "change #tags": "changeTags",
      "click .change-type": "changeType",
      "click .commit-subject": "selectCommit",
      "click .commit-info-icon": "selectCommit"
    };
  }

  init() {
    this.selectedTags = new ReactiveVar([]);
    this._selectedCommit = new ReactiveVar();
    // this.set("ready", true);
    this.set("type", "graph");
    this._resumeGraph = new Graph();
    this.autorun(() => {
      this.subscribe("commits", () => {
        Commits.find(
          {},
          { sort: { date: 1 } }
        )
        .forEach((commit) => {
          let branch = this._resumeGraph.branches.get(commit.branchName);
          if (!branch) {
            const parent = commit.parentBranchName ? this._resumeGraph.branches.get(commit.parentBranchName) : this._resumeGraph;
            branch = parent.branch(commit.branch || commit.branchName);
          }
          if (commit.mergeCommitId) {
            const mergeCommit = this._resumeGraph.getCommit(commit.mergeCommitId);
            branch.merge(
              mergeCommit.branch,
              {
                _id: commit._id,
                date: commit.dateLabel || commit.date,
                showDate: commit.showDate,
                tags: commit.tags,
                subject: commit.subject,
                body: commit.body
              },
              commit.close
            );
          }
          else {
            branch.commit({
              _id: commit._id,
              subject: commit.subject,
              body: commit.body,
              showDate: commit.showDate,
              tags: commit.tags,
              date: commit.dateLabel || commit.date
            });
          }
        });
        this.set("ready", true);
      });
    });
  }

  rendered() {
    if (Meteor.isClient) {
      this.on(window, "scroll", () => {
        this.animateElements();
      });
      this.on(window, "resize", () => {
        this.animateElements();
      });

      const $tags = this.$("#tags");
      $tags.select2({
        data: [
          { text: "Research", id: "research" },
          { text: "Education", id: "education" },
          { text: "Work", id: "work" },
          { text: "Open Source", id: "open-source" }
        ],
        placeholder: $tags.attr("placeholder"),
        containerCssClass: $tags.attr("class"),
        multiple: true
      });

      this.autorun((comp) => {
        if (this.get("ready")) {
          Tracker.afterFlush(() => {
            BlazeLayout.initialRenderComplete();
          });
          comp.stop();
        }
      });
    }
  }

  animateElements() {
    if (!Meteor.isClient) {
      return;
    }
    const textElems = _.toArray(this.$(".commit-content"));

    textElems.forEach((elem) => {
      const $elem = $(elem);
      const path = $elem.find("path")[0];
      if (path && isElementInViewport(path)) {
        $elem.addClass("visible");
      }
      else {
        $elem.removeClass("visible");
      }
    });
  }

  resumeElements() {
    const types = ["line", "dot", "text", "g"];
    if (!this.get("ready")) {
      return [];
    }
    Meteor.RouteLoading && Meteor.RouteLoading.set(true);
    Tracker.afterFlush(() => {
      this.animateElements();
      Meteor.RouteLoading && Meteor.RouteLoading.set(false);
    });
    const tags = this.selectedTags.get();
    const graph = tags ? this._resumeGraph.getSubGraph({ tags }) : this._resumeGraph;
    const ret = {
      children: _.sortBy(graph.computeGraph(), g => types.indexOf(g.type))
    };
    ret.height = ret.children[0].y + 50;
    return ret;
  }

  templateName(branch) {
    return `resume_${branch.name}`;
  }

  graph() {
    return this.get("type") === "graph";
  }

  branches() {
    if (!this.get("ready")) {
      return [];
    }

    return _.sortBy(
      [
        this._resumeGraph.branches.get("waterloo"),
        this._resumeGraph.branches.get("glamorgan"),
        // this._resumeGraph.branches.get("college"),
        // this._resumeGraph.branches.get("blugem"),
        this._resumeGraph.branches.get("vworker"),
        this._resumeGraph.branches.get("bt"),
        this._resumeGraph.branches.get("eagleeye"),
        this._resumeGraph.branches.get("mobiplus"),
        this._resumeGraph.branches.get("mushroomcloud"),
        this._resumeGraph.branches.get("justplay"),
        this._resumeGraph.branches.get("irdeto"),
        this._resumeGraph.branches.get("qnx")
      ],
      (branch) => {
        let date = branch.lastCommit.date;
        if (!date) {
          date = branch.lastCommit.parentCommit.date;
        }
        if (date === "Current") {
          return new Date();
        }
        return date;
      }
    ).reverse();
  }

  dateRange(branch) {
    let endDate = branch.lastCommit.date;
    if (!endDate) {
      endDate = branch.lastCommit.parentCommit.date;
    }

    return `${moment(branch.firstCommit.date).format("MMM YYYY")} - ${_.isDate(endDate) ? moment(endDate).format("MMM YYYY") : endDate}`;
  }

  modalTitle(commit) {
    if (!commit) {
      return "";
    }
    if (commit.body || this.get("cameFromInfoIcon")) {
      return commit.subject;
    }
    return commit.branch.firstCommit.subject;
  }

  selectedCommit() {
    return this._selectedCommit.get();
  }

  cameFromInfoIcon() {
    return this.get("cameFromInfoIcon");
  }

  changeTags() {
    this.selectedTags.set(this.$("#tags").val());
  }

  changeType(e) {
    this.set("type", $(e.currentTarget).data("type"));
  }

  selectCommit(e) {
    const $target = $(e.currentTarget);
    const commitId = $target.closest("g").attr("id");
    const commit = this._resumeGraph.getCommit(commitId);
    this._selectedCommit.set(commit);
    const cameFromIcon = $target.hasClass("commit-info-icon");
    this.set("cameFromInfoIcon", cameFromIcon);
    if (cameFromIcon || commit.body || Template[`resume_${commit.branch.name}`]) {
      this.$(".modal").modal("show");
    }
  }
}

BlazeComponent.register(Template.resume, ResumeView);
