import _ from "underscore";
import moment from "moment";
import { Random } from "meteor/random";

class Dot {
  constructor(obj) {
    _.extend(this, obj);
    this.type = "dot";
    this.isDot = true;
  }

  attr() {
    return {
      transform: `translate(${this.x},${this.y})`,
      cx: 0,
      cy: 0,
      r: this.r,
      fill: this.color
    };
  }
}

class Text {
  constructor(obj) {
    _.extend(this, obj);
    this.type = "text";
    this.isText = true;
  }

  attr() {
    return {
      transform: this.transform,
      class: this.class,
      x: this.x,
      "data-desired-x": this.x,
      y: this.y,
      fill: this.color
    };
  }
}

class G {
  constructor(obj) {
    _.extend(this, obj);
    this.type = "g";
    this.isG = true;
  }

  attr() {
    const self = this;
    return {
      transform() {
        return `translate(${self.x}, ${self.y})`;
      }
    };
  }
}

class Line {
  constructor(obj) {
    _.extend(this, obj);
    this.type = "line";
    this.isLine = true;
  }

  d() {
    const g = this;
    if (g.x1 === g.x2 || g.y1 === g.y2) {
      return `M ${g.x1},${g.y1} L ${g.x2},${g.y2}`;
    }
    const cx1 = g.x1 - this.graph.radius / 2;
    const cy1 = g.y1 - this.graph.verticalSpacing / 2;
    const cx2 = g.x2 + this.graph.radius / 2;
    const cy2 = g.y1 - this.graph.verticalSpacing / 2;
    return `M ${g.x1},${g.y1} C ${cx1}, ${cy1}, ${cx2}, ${cy2} ${g.x2}, ${g.y2}`;
  }

  attr() {
    return {
      stroke: this.color,
      fill: "transparent",
      "stroke-width": this.lineWidth || this.graph.lineWidth,
      "stroke-dasharray": this.dashes || "",
      d: this.d()
    };
  }
}

class Commit {
  constructor(branch, parentCommit, {
    _id = Random.id(), subject, body, date, tags = [], showDate = date !== undefined
  }) {
    this.branch = branch;
    this.parentCommit = parentCommit;
    this.body = body;
    this.showDate = showDate;
    this.date = date;
    this.mergeCommits = [];
    this.subject = subject;
    this.tags = tags;
    this.isG = true;
    this._id = _id;
    if (parentCommit) {
      parentCommit.childCommit = this;
    }
  }

  attr() {
    const self = this;
    return {
      transform() {
        return `translate(0, ${self.contentY})`;
      },
      class: "commit-content"
    };
  }

  children() {
    const ret = [];
    ret.push(new Text({
      _id: `${this._id}-commit-subject`,
      text: this.subject,
      class: "commit-subject",
      color: this.branch.color,
      x: this.contentX,
      y: this.branch.graph.radius / 3,
      transform: `translate(${1000 - this.contentX}, 0)`
    }));
    ret.push(new Text({
      text: "\uf05a",
      _id: `${this._id}-commit-info-icon`,
      class: "commit-info-icon",
      color: this.branch.color,
      x: this.contentX,
      y: this.branch.graph.radius / 3,
      transform: `translate(${1000 - this.contentX}, 0)`
    }));

    if (this.subject) {
      ret.push(new Line({
        _id: `${this._id}-line-to-subject`,
        graph: this,
        x1: this.x,
        y1: 0,
        x2: this.contentX,
        y2: 0,
        dashes: "2 2",
        lineWidth: 1,
        color: this.branch.color
      }));
    }

    if (this.showDate) {
      ret.push(new Text({
        _id: `${this._id}-commit-date`,
        class: "commit-date",
        text: this.date instanceof Date ? moment(this.date).format(this.branch.graph.dateFormat) : this.date,
        x: -95,
        y: this.branch.graph.radius / 3,
        color: "#ccc",
        transform: "translate(-65, 0)"
      }));

      ret.push(new Line({
        _id: `${this._id}-line-to-date`,
        graph: this,
        x1: this.x,
        y1: 0,
        x2: -35,
        y2: 0,
        dashes: "2 2",
        lineWidth: 1,
        color: "#ccc"
      }));
    }
    return ret;
  }
}

class Branch {
  static Colors = ["red", "green", "blue", "black", "yellow"];

  static Id = 0;

  constructor(graph, parentBranch, {
    name, color, label, description
  }) {
    this.name = name;
    this.parentBranch = parentBranch;
    this.description = description;
    this.label = label;
    this.color = color || Branch.Colors[Branch.Id++ % Branch.Colors.length];
    this.graph = graph;
    this.commits = [];
  }

  clone() {
    return new Branch(this.graph, this.parentBranch, { name: this.name, color: this.color });
  }

  commit(message) {
    this.isActive = true;
    this.lastCommit = new Commit(this, this.lastCommit || (this.parentBranch && this.parentBranch.lastCommit), _.isString(message) ? { subject: message } : message);
    this.commits.push(this.lastCommit);
    this.graph.commits.set(this.lastCommit._id, this.lastCommit);
    if (!this.firstCommit) {
      this.firstCommit = this.lastCommit;
    }
    const active = Array.from(this.graph.branches.values())
    .filter(b => b.isActive)
    .map((branch, branchIndex) => ({
      branch,
      branchIndex,
      commit: branch === this ? this.lastCommit : undefined
    }));
    this.lastCommit.timeSeriesIndex = this.graph.timeSeries.length;
    this.graph.maxActive = Math.max(this.graph.maxActive, active.length);
    this.graph.timeSeries.push(active);
    return this;
  }

  tag(tags) {
    tags = _.isArray(tags) ? tags : [tags];
    this.lastCommit.tags = tags;
    return this;
  }

  close() {
    this.isActive = false;
  }

  merge(branch, message, close = true) {
    if (close) {
      branch.isActive = false;
    }
    this.commit(message);
    branch.lastCommit.didClose = close;
    this.lastCommit.mergeCommits.push(branch.lastCommit);
    return this;
  }

  branch(name) {
    const branch = new Branch(this.graph, this, _.isString(name) ? { name } : name);
    this.graph.branches.set(_.isString(name) ? name : name.name, branch);
    return branch;
  }
}


export class Graph {
  constructor() {
    this.branches = new Map();
    this.maxActive = 0;
    this.timeSeries = [];
    this.verticalSpacing = 50;
    this.horizontalSpacing = 50;
    this.dateFormat = "YYYY-MM";
    this.commits = new Map();
    this.radius = 15;
    this.lineWidth = 8;
  }

  branch(name) {
    const branch = new Branch(this, null, _.isString(name) ? { name } : name);
    this.branches.set(_.isString(name) ? name : name.name, branch);
    return branch;
  }

  getSubGraph({
    branchNames = [],
    tags = []
  } = {}) {
    const newGraph = new Graph();
    this.timeSeries.forEach((row) => {
      const commit = row.find(r => r.commit).commit;
      const oldBranch = commit.branch;
      if ((branchNames.length === 0 || branchNames.includes(oldBranch.name)) && !newGraph.branches.get(oldBranch.name)) {
        newGraph.branches.set(
          oldBranch.name,
          new Branch(
            newGraph,
            oldBranch.parentBranch && newGraph.branches.get(oldBranch.parentBranch.name),
            { name: oldBranch.name, color: oldBranch.color }
          )
        );
      }

      // NOTE: we aren't interested in this commit directly,
      // but if it's a merge commit, and we've referenced both branches, lets merge it in.
      let skipCommit = tags.length !== 0 && _.intersection(commit.tags, tags).length === 0;
      if (skipCommit && commit.mergeCommits.length) {
        if (newGraph.branches.get(commit.branch.name) && newGraph.branches.get(commit.mergeCommits[0].branch.name)) {
          skipCommit = !newGraph.branches.get(commit.branch.name).lastCommit || !newGraph.branches.get(commit.mergeCommits[0].branch.name).lastCommit;
        }
      }
      const newBranch = newGraph.branches.get(oldBranch.name);
      if (!newBranch || skipCommit) {
        if (commit.mergeCommits.length) {
          const closingCommit = commit.mergeCommits[0];
          const closingBranch = newGraph.branches.get(closingCommit.branch.name);
          if (closingBranch) {
            closingBranch.isActive = closingBranch.isActive && !closingCommit.didClose;
          }
        }
        return;
      }
      if (commit.mergeCommits.length) {
        commit.mergeCommits.forEach((mc) => {
          const newMergedBranch = newGraph.branches.get(mc.branch.name);
          if (!newMergedBranch) {
            return;
          }
          newBranch.merge(
            newMergedBranch,
            {
              _id: commit._id,
              subject: commit.subject,
              body: commit.body,
              date: commit.date,
              showDate: commit.showDate,
              tags: commit.tags
            },
            mc.didClose
          );
        });
      }
      else {
        newBranch.commit({
          _id: commit._id,
          subject: commit.subject,
          body: commit.body,
          date: commit.date,
          showDate: commit.showDate,
          tags: commit.tags
        });
      }
    });

    return newGraph;
  }

  getFilteredTimeSeries(branches = []) {
    this.maxActive = 0;
    return this.timeSeries
    .filter(row => row.find(item => branches.includes(item.branch) && item.commit))
    .map((row) => {
      const newRow = row.filter(item => branches.includes(item.branch));
      newRow.forEach((item, index) => {
        item.branchIndex = index;
      });
      this.maxActive = Math.max(this.maxActive, newRow.length);
      return newRow;
    });
  }

  getCommit(commitId) {
    return this.commits.get(commitId);
  }

  computeGraph() {
    const timeSeries = this.timeSeries;
    return _.flatten(timeSeries.map((row, ri) => {
      const rowIndex = timeSeries.length - ri;
      const rowRet = row.map((item) => {
        const branchIndex = item.branchIndex;
        if (item.commit) {
          item.commit.x = branchIndex * this.horizontalSpacing;
          item.commit.y = rowIndex * this.verticalSpacing;

          item.commit.contentX = this.maxActive * this.horizontalSpacing;
          item.commit.contentY = rowIndex * this.verticalSpacing;
          const ret = [
            new Dot({
              _id: `${item.commit._id}-commit-dot`,
              x: branchIndex * this.horizontalSpacing,
              y: rowIndex * this.verticalSpacing,
              color: item.branch.color,
              r: this.radius
            }),
            item.commit
          ];

          if (item.commit.parentCommit && (item.commit.parentCommit.timeSeriesIndex === item.commit.timeSeriesIndex - 1 || item.commit.branch !== item.commit.parentCommit.branch)) {
            ret.push(new Line({
              graph: this,
              x1: item.commit.parentCommit.branch.x,
              y1: ((rowIndex + 1) * this.verticalSpacing),
              x2: branchIndex * this.horizontalSpacing,
              y2: (rowIndex * this.verticalSpacing),
              color: item.branch.color
            }));
          }
          else if (item.branch.y) {
            ret.push(new Line({
              graph: this,
              x1: item.branch.x || 0,
              y1: item.branch.y || 0,
              x2: branchIndex * this.horizontalSpacing,
              y2: rowIndex * this.verticalSpacing,
              color: item.branch.color
            }));
          }

          item.commit.mergeCommits.forEach((commit) => {
            ret.push(new Line({
              graph: this,
              x1: commit.branch.x || 0,
              y1: commit.branch.y || 0,
              x2: branchIndex * this.horizontalSpacing,
              y2: (rowIndex * this.verticalSpacing),
              color: commit.branch.color
            }));
          });
          item.branch.x = branchIndex * this.horizontalSpacing;
          return ret;
        }

        const ret = [new Line({
          graph: this,
          x1: item.branch.x || 0,
          y1: item.branch.y || 0,
          x2: branchIndex * this.horizontalSpacing,
          y2: rowIndex * this.verticalSpacing,
          color: item.branch.color
        })];
        item.branch.x = branchIndex * this.horizontalSpacing;
        return ret;
      });
      row.forEach((item) => {
        item.branch.y = rowIndex * this.verticalSpacing;
      });
      return rowRet;
    }));
  }

  populateSvg(svg) {
    const graphComponents = this.computeGraph();
    const types = ["line", "dot", "text"];
    _.sortBy(graphComponents, g => types.indexOf(g.type)).forEach((g) => {
      if (g.type === "dot") {
        const dot = $(document.createElementNS("http://www.w3.org/2000/svg", "circle"));
        dot.attr("cx", g.x);
        dot.attr("cy", g.y);
        dot.attr("r", this.radius);
        dot.attr("fill", g.color);
        svg.append(dot);
      }
      else if (g.type === "line") {
        const line = $(document.createElementNS("http://www.w3.org/2000/svg", "path"));
        if (g.x1 === g.x2) {
          line.attr("d", `M ${g.x1},${g.y1} L ${g.x2},${g.y2}`);
        }
        else {
          const cx1 = g.x1 - this.radius / 2;
          const cy1 = g.y1 - this.verticalSpacing / 2;
          const cx2 = g.x2 + this.radius / 2;
          const cy2 = g.y1 - this.verticalSpacing / 2;
          line.attr("d", `M ${g.x1},${g.y1} C ${cx1}, ${cy1}, ${cx2}, ${cy2} ${g.x2}, ${g.y2}`);
        }
        line.attr("fill", "transparent");
        line.attr("stroke-width", 5);
        line.attr("stroke", g.color);

        svg.append(line);
      }
      else if (g.type === "text") {
        const text = $(document.createElementNS("http://www.w3.org/2000/svg", "text"));
        text.attr("x", g.x);
        text.attr("y", g.y);
        text.text(g.text);
        svg.append(text);
      }
    });
  }
}
