
import _ from "underscore";
import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
import { Notifications } from "meteor/gfk:notifications";
import "../resume/segments/justplay.html";
import "../resume/segments/mobiplus.html";
import "../resume/segments/mushroomcloud.html";
import "../resume/segments/qnx.html";
import "../resume/segments/vworker.html";
import "../resume/segments/bt.html";
import "../resume/segments/eagleeye.html";
import "../resume/segments/irdeto.html";
import "./index.html";
import "../../components/wordcloud/wordcloud.js";

export class IndexView extends BlazeComponent {
  static HelperMap() {
    return [
      "words",
      "uniqueWords",
      "isClient"
    ];
  }

  init() {
    const splitterRegex = /[^a-zA-Z\-'"]+/;
    const words = _.flatten([
      $(Blaze.toHTML(Template.resume_eagleeye)).text().split(splitterRegex),
      $(Blaze.toHTML(Template.resume_justplay)).text().split(splitterRegex),
      $(Blaze.toHTML(Template.resume_mushroomcloud)).text().split(splitterRegex),
      $(Blaze.toHTML(Template.resume_irdeto)).text().split(splitterRegex),
      $(Blaze.toHTML(Template.resume_bt)).text().split(splitterRegex),
      $(Blaze.toHTML(Template.resume_vworker)).text().split(splitterRegex),
      $(Blaze.toHTML(Template.resume_mobiplus)).text().split(splitterRegex),
      $(Blaze.toHTML(Template.resume_qnx)).text().split(splitterRegex)
    ])
    .filter(w => !w.match(/(technologies|used)/i));
    this._words = new ReactiveVar([]);

    Meteor.call("getUniqueArticleWords", (err, res) => {
      if (err) {
        Notifications.error("Couldn't get article words", err.reason, { timeout: 5000 });
      }
      else {
        this._words.get().push(...words);
        this._words.get().push(...res);
        this._words.dep.changed();
      }
    });
  }

  isClient() {
    return Meteor.isClient;
  }

  rendered() {
    Meteor.RouteLoading && Meteor.RouteLoading.set(false);

    const initInteval = this.setInterval(() => {
      if (this.$("#wordcloud text").length) {
        this.clearInterval(initInteval);
        BlazeLayout.initialRenderComplete();
      }
    }, 10);
  }

  uniqueWords() {
    return _.unique(this.words());
  }

  words() {
    return this._words.get();
  }
}
BlazeComponent.register(Template.index, IndexView);
