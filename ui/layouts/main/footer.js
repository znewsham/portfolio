import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { FlowRouter } from "meteor/kadira:flow-router";
import "./footer.html";

export class MainFooter extends BlazeComponent {
  static HelperMap() {
    return [
      "bitbucketUrl"
    ];
  }

  bitbucketUrl() {
    if (FlowRouter.watchPathChange) {
      FlowRouter.watchPathChange();
      return FlowRouter.current().route.options.bitbucketUrl && FlowRouter.current().route.options.bitbucketUrl();
    }
  }
}

BlazeComponent.register(Template.footer, MainFooter);
