import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { FlowRouter } from "meteor/kadira:flow-router";
import "./header.js";
import "./footer.js";
import "../../components/loading/loading.js";
import "./main.html";

export class MainLayout extends BlazeComponent {
  static HelperMap() {
    return [
      "isLoading",
      "class"
    ];
  }

  init() {
    this.autorun(() => {
      if (Meteor.isClient && !Meteor.userId() && !Meteor.loggingIn()) {
        Meteor.loginAsGuest((err, res) => {
          if (err) {
            console.error(err);
          }
        });
      }
      else if (Meteor.isClient && Meteor.userId()) {
        this.subscribe("self");
      }
    });

    if (Meteor.isClient) {
      this.autorun(() => {
        const title = this.title();
        if (title) {
          document.title = title;
        }
      });
    }
  }

  class(container) {
    if (container !== false) {
      return "container";
    }
  }

  title() {
    FlowRouter.watchPathChange();
    return FlowRouter.current().route.options.title && FlowRouter.current().route.options.title();
  }

  isLoading() {
    return Meteor.RouteLoading && Meteor.RouteLoading.get();
  }
}
BlazeComponent.register(Template.mainLayout, MainLayout);
