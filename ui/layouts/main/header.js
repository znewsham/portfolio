import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { FlowRouter } from "meteor/kadira:flow-router";
import { Notifications } from "meteor/gfk:notifications";
import "./header.html";

export class MainHeader extends BlazeComponent {
  static HelperMap() {
    return [
      "items",
      "isActive",
      "bitbucketUrl",
      "isAdmin",
      "disabledIfNotAdmin",
      "isClient"
    ];
  }

  static EventMap() {
    return {
      "mousedown #save-btn-wrapper": "showSaveWarning",
      "mouseover #save-btn-wrapper": "showSaveWarning"
    };
  }

  isClient() {
    return Meteor.isClient;
  }

  isActive(item) {
    if (Meteor.isClient) {
      FlowRouter.watchPathChange();
      return FlowRouter.current().path === item.href;
    }
  }

  items() {
    return [
      { text: "Resume", href: "/resume" },
      { text: "Research", href: "/articles/research" },
      { text: "Open Source", href: "/articles/open-source" },
      { text: "Work Experience", href: "/articles/work-experience" },
      {
        text: "Projects",
        items: [
          { text: "Dynamic Tables", href: "/article/dynamic-tables" }
        ]
      }
    ];
  }

  showSaveWarning() {
    this.$("#save-btn-wrapper").tooltip("show");
  }

  disabledIfNotAdmin() {
    return this.isAdmin() ? {} : { disabled: "disabled" };
  }

  isAdmin() {
    return Meteor.isClient && Meteor.user() && Meteor.user().isAdmin;
  }
}

BlazeComponent.register(Template.header, MainHeader);
