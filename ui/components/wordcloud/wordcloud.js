import d3 from "d3";
import cloud from "d3-cloud";
import sw from "stopword";
import _ from "underscore";
import pluralize from "pluralize";
import { BlazeComponent } from "meteor/znewsham:blaze-component";
import { Random } from "meteor/random";
import "./wordcloud.html";

let createCanvas;
if (Meteor.isServer) {
  createCanvas = require("canvas").createCanvas;
}

function draw(layout, words) {
  d3.select(this.$("#wordcloud")[0]).append("svg")
  .attr("width", layout.size()[0])
  .attr("height", layout.size()[1])
  .append("g")
  .attr("transform", `translate(${layout.size()[0] / 2},${layout.size()[1] / 2})`)
  .selectAll("text")
  .data(words)
  .enter()
  .append("text")
  .style("font-size", d => `${d.size}px`)
  .style("font-family", "Impact")
  .attr("text-anchor", "middle")
  .attr("transform", d => `translate(${[d.x, d.y]})rotate(${d.rotate})`)
  .text(d => d.text);
}

function getWords(words, minFontSize, maxFontSize) {
  words = words
  .filter(w => (w.word || w).replace(/[^a-zA-Z]*/g, "").replace(/[^\w]*/g, "").length)
  .map((w) => {
    if (w.word) {
      w.word = pluralize.singular(w.word);
      return w;
    }
    return pluralize.singular(w);
  })
  .filter(w => (w.word || w).length >= 2);
  const countWords = words.reduce((memo, word) => {
    let actualWord = word;
    let count = 1;
    if (_.isObject(word)) {
      actualWord = word.word;
      count = word.count;
    }
    if (sw.removeStopwords([actualWord]).length) {
      if (!memo[actualWord.toLowerCase()]) {
        memo[actualWord.toLowerCase()] = {
          actualWord: actualWord.slice(0, 1).toUpperCase() + actualWord.slice(1),
          count
        };
      }
      else {
        memo[actualWord.toLowerCase()].count += count;
      }
    }
    return memo;
  }, { });
  const largest = Math.max(..._.values(countWords).map(c => c.count));
  const smallest = 1;
  const sorted = _.sortBy(_.map(countWords, (val, word) => ({
    text: val.actualWord, size: minFontSize + ((maxFontSize - minFontSize) * (val.count / (largest - smallest)))
  })), e => -e.size).slice(0, 100);
  return sorted;
}


export class WordcloudComponent extends BlazeComponent {
  rendered() {
    const random = Random.createWithSeeds(24);
    const {
      words, width, height, minFontSize, maxFontSize
    } = this.nonReactiveData();
    const layout = cloud()
    .size([width, height])
    .canvas(() => (Meteor.isClient ? document.createElement("canvas") : createCanvas(0, 0)))
    .words(getWords(words, minFontSize, maxFontSize))
    .random(() => random.fraction())
    .padding(2)
    .rotate(() => 0)
    .font("Impact")
    .spiral("archimedean")
    .fontSize(d => d.size)
    .on("end", words => draw.call(this, layout, words));
    layout.start();
  }
}

BlazeComponent.register(Template.wordcloud, WordcloudComponent);
