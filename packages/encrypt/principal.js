/*
  Workflow:
  A user has:
    A derived (from their password) symmetric key
    A randomly generated master asymmetric encryption keypair
    A randomly generated "usage" asymmetric encryption keypair

  The user's master private key is encrypted with their derived key and saved on the user object.
  The user's "usage" private key is encrypted with their master public key and (along with the usage public key) saved as a principal.

  If the user wants to change their password, they need only re-encrypt their master key.

  If the user needs to reset their password, they lose access to all content they had access to,
  until another user with access grants them access.

  When a user wants to write some new content:
    They generate a new symmetric key
    They encrypt the symmetric key with their usage public key
    The encrypted symmetric key + principalId of the encrypting entity is added to a new principal
    They encrypt the content with the symmetric key, set the principalId to that of the newly created principal, and save it to the DB.

  When a user wants to decrypt some content: (assuming they have direct access)
    They fetch the item from the DB, getting the principal ID
    They decrypt the symmetric key with their usage private key

  When a user wants to decrypt some content: (assuming the DONT have direct access)
    They fetch the item from the DB, getting the principal ID.
    They fetch all the principals the user has direct access to.
    It is likely that one of these has direct access to the content.
    If not, find all the principals that the users direct principals have access to, and keep going.

  When a user wants to edit some existing content:
    They decrypt the content (fetching the key)
    They encrypt the new content with the same symmetric key

  When a user wants to share some encrypted content with a user:
    They must know the users usage principalId.
    They encrypt the symmetric key with the users usage public key
    They add the users { principalId, encrypted symmetric key } to the principal

  When a user wants to share some encrypted content with a group of users:
    They must know the users' ueage principalIds
    They create a new principal, with a new asymetric keypair.
    They encrypt the private key of the principal once for each of the users, and add it to the principal
    They save the principal

  When a user wants to remove another users (or groups) access
    They must know the uesr's usage principalIds
    If the content principal's (encrypted) decryption key, for the user's principalId has never been accessed, simply remove.
    If it HAS been acceessed, generate a new encryption key, re-encrypt the content, then save the encrypted symmetric key once per user.
*/

/** Signing Workflow:

  The initial user (Alice) creates an account. They are the initial source of trust (as the account owner)

  The problem is allowing new users to create accounts, and allow them to request access from Alice
  and for Alice to know that the received public key belongs to the user she invited

  Alice can invite other users (e.g., Bob)
  upon inviting Bob a principal is created for Bob using an encryption key pair generated by Alice
  Alice then adds herself to Bob's principals `principalAccess`.
  When Bob accepts the invitation (and creates his password), he has no access to his own Principal
  Bob requests access to his principal from Alice

*/
function generateSymmetricEncryptionKey() {
  return crypto.subtle.generateKey(
    {
      name: "AES-GCM",
      length: 256
    },
    true,
    ["encrypt", "decrypt"]
  );
}

function generateAssymetricEncryptionKeyPair() {
  return crypto.subtle.generateKey(
    {
      name: "RSA-OAEP",
      modulusLength: 2048,
      publicExponent: new Uint8Array([1, 0, 1]),
      hash: "SHA-256"
    },
    true,
    ["encrypt", "decrypt"]
  );
}

function argsFromJWK(jwk) {
  const name = jwk.alg;
  if (name.startsWith("RSA")) {
    return {
      name: name.split("-").slice(0, 2).join("-"),
      hash: `SHA-${name.split("-").slice(-1)[0]}`
    };
  }
  if (name.startsWith("A")) {
    return {
      name: `AES-${name.slice(-3)}`,
      length: parseInt(name.slice(1, 4), 10)
    };
  }
}

async function deriveKeyFromPassword(password, salt = "no-salt?") {
  const enc = new TextEncoder();
  const keyMaterial = await crypto.subtle.importKey(
    "raw",
    enc.encode(password),
    { name: "PBKDF2" },
    false,
    ["deriveBits", "deriveKey"]
  );

  const key = await window.crypto.subtle.deriveKey(
    {
      name: "PBKDF2",
      iterations: 100000,
      salt: enc.encode(salt),
      hash: "SHA-256"
    },
    keyMaterial,
    {
      name: "AES-GCM",
      length: 256
    },
    true,
    ["wrapKey", "unwrapKey", "encrypt", "decrypt"]
  );

  return key;
}

const encoder = new TextEncoder();
const decoder = new TextDecoder();

/**
  a Principal is a member of the encryption/signing graph, it may contain a combination of:
    assym encryption keys OR symmetric encryption keys
    assym signing keys


  The Principal schema is as follows:
  {
    _id: String
    principalAccess: [PrincipalAccessSchema]
    certificates: [PrincipalCertificate]
    _iv: Binary,
    encryptionKey: CryptoKey (optional, can't have this AND _symmetricKey)
    verificationKey: CryptoKey (optional)
    _symmetricKey: CryptoKey (optional, private, can't have this AND _encryptionKey)
    _signingKey: CryptoKey (optional, private)
    _decryptionKey: CryptoKey (optional, private)
  }

  When stored in the DB, the Principal drops it's private fields, and in their place stores:
  {
    _id: string,
    principalAccess: [PrincipalAccessSchema],
    certificates: [PrincipalCertificate],
    _iv: Binary,
    encryptionKey: JWK JSON string,
    verificationKey: JWK JSON string
  }

  a PrincipalAccessSchema contains the following:
  {
    principalId: String,
    type: String (sign|decrypt)
    key: Binary (encrypted),
    args: Object (optional)
  }
*/
export class Principal {
  constructor(_id) {
    this._id = _id;
    this.principalAccess = [];
  }

  async importEncryptionKey(key) {
    this.setEncryptionKey(await crypto.subtle.importKey(
      "jwk",
      key,
      argsFromJWK(key),
      true,
      key.key_ops
    ));
  }

  async importVerificationKey(key) {
    this.setVerificationKey(await crypto.subtle.importKey(
      "jwk",
      key,
      argsFromJWK(key),
      true,
      key.key_ops
    ));
  }

  setDecryptionKey(key) {
    if (!key || (key.type !== "secret" && key.type !== "private")) {
      throw new Error("Must provide a key");
    }
    this._decryptionKey = key;
  }

  setEncryptionKey(key) {
    if (!key || (key.type !== "secret" && key.type !== "public")) {
      throw new Error("Must provide a key");
    }

    if (key.type === "secret") {
      this._symmetricKey = key;
    }
    else {
      this.encryptionKey = key;
    }
  }

  setVerificationKey(key) {
    if (!key || key.type !== "public") {
      throw new Error("Must provide a key");
    }
    this.verificationKey = key;
  }

  setSigningKey(key) {
    if (!key || key.type !== "private") {
      throw new Error("Must provide a key");
    }
    this._signingKey = key;
  }

  getEncryptionKey() {
    return this.encryptionKey;
  }

  getSecretKey() {
    return this._symmetricKey;
  }

  getDecryptionKey() {
    return this._decryptionKey;
  }

  getSigningKey() {
    return this._signingKey;
  }

  getVerificationKey() {
    return this.verificationKey;
  }


  async unwrapKey({ key, args }) {
    const myDecKey = this.getDecryptionKey() || this.getSecretKey();
    if (!myDecKey) {
      throw new Error("You don't have access to that yet");
    }

    const decrypted = await this.decrypt(key, myDecKey, true);
    const jwk = JSON.parse(decrypted);
    return crypto.subtle.importKey(
      "jwk",
      jwk,
      argsFromJWK(jwk),
      true,
      jwk.key_ops
    );
  }

  async unwrapAccess(pa) {
    const myDecKey = this.getDecryptionKey() || this.getSecretKey();
    if (!myDecKey || !myDecKey.type) {
      throw new Error("This principal can't wrap");
    }

    try {
      const tmkKeyJwk = JSON.parse(await this.decrypt(pa.tmpKey, myDecKey, true, pa.iv));
      const tmpKey = await crypto.subtle.importKey(
        "jwk",
        tmkKeyJwk,
        argsFromJWK(tmkKeyJwk),
        true,
        tmkKeyJwk.key_ops
      );
      pa = JSON.parse(await this.decrypt(pa.access, tmpKey));

      return {
        principalId: pa.toPrincipalId,
        type: pa.type,
        key: await crypto.subtle.importKey(
          "jwk",
          pa.key,
          argsFromJWK(pa.key),
          true,
          pa.key.key_ops
        )
      };
    }
    catch(e) {
      console.error(e);
    }
  }

  async wrapAccessRequest({ toPrincipalId, key, type }) {
    const myEncKey = this.getEncryptionKey() || this.getSecretKey();
    if (!myEncKey) {
      throw new Error("This principal can't wrap");
    }
    const encoded = encoder.encode(JSON.stringify({
      key: await crypto.subtle.exportKey("jwk", key),
      toPrincipalId,
      type
    }));

    const tmpEncKey = await generateSymmetricEncryptionKey();
    const iv = window.crypto.getRandomValues(new Uint8Array(12));
    return {
      tmpKey: await this.encrypt(encoder.encode(JSON.stringify(await crypto.subtle.exportKey("jwk", tmpEncKey))), myEncKey, iv),
      iv,
      access: await this.encrypt(encoded, tmpEncKey)
    };
  }

  async wrapKey(key) {
    const myEncKey = this.getEncryptionKey() || this.getSecretKey();
    if (!myEncKey) {
      throw new Error("This principal can't wrap");
    }
    const encoded = encoder.encode(JSON.stringify(await crypto.subtle.exportKey("jwk", key)));

    const encrypted = await this.encrypt(encoded, myEncKey);
    return {
      key: encrypted
    };
  }

  lock() {
    delete this._decryptionKey;
    delete this._signingKey;
  }

  isUnlocked() {
    return !!(this._symmetricKey || this._decryptionKey || this._signingKey);
  }

  async init() {
    if (this.inited) {
      return;
    }
    if (!this.iv) {
      this.iv = window.crypto.getRandomValues(new Uint8Array(12));
    }
    this.inited = true;
    if (!this.isUnlocked()) {
      throw new Error("You can't init a locked principal");
    }
    if (this._symmetricKey && !this._symmetricKey.type) {
      this._symmetricKey = await this.unwrapKey(this._symmetricKey);
    }
    for (let i = 0; i < this.principalAccess.length; i++) {
      this.principalAccess[i] = await this.unwrapAccess(this.principalAccess[i].encryptedToPrincipalIdAndKey);
    }
  }

  async unlockWith(principal) {
    if (this.getDecryptionKey()) {
      throw new Error("Already unlocked");
    }
    await principal.init();
    const principalKey = principal.principalAccess.find(p => p.principalId === this._id);
    if (!principalKey) {
      throw new Error("You don't have access to that");
    }
    if (principalKey.type === "decrypt") {
      this.setDecryptionKey(principalKey.key);
      await this.init();
    }
    else if (principal.type === "sign") {
      this.setSigningKey(principalKey.key);
    }
  }

  async hasAccessTo(principal) {
    if (!this.getDecryptionKey()) {
      throw new Error("You don't have access to that yet");
    }
    await this.init();
    if (!principal) {
      throw new Error("You must provide a principal");
    }
    return this.principalAccess.find(p => p.principalId === principal._id);
  }

  async grantAccessFor(principal) {
    const accessRequest = await principal.wrapAccessRequest({
      key: this.getDecryptionKey() || this.getSecretKey(),
      type: "decrypt",
      toPrincipalId: this._id
    });
    principal.principalAccess.push({
      principalId: this._id,
      type: "decrypt",
      key: this.getDecryptionKey() || this.getSecretKey()
    });

    return accessRequest;
  }

  async authorizeWith(principal) {
    let cert = this.certificates.find(aCert => aCert.principalId === principal._id);
    if (cert) {
      throw new Error("The selected principal has already autorized this principal");
    }

    const keys = [];
    if (this.encryptionKey) {
      keys.push(this.encryptionKey);
    }
    if (this.verificationKey) {
      keys.push(this.verificationKey);
    }
    cert = new Certificate({
      keys
    });

    cert.setSignature(await principal.sign(cert));
    this.certificates.push(cert);
  }

  async encrypt(content, key, iv) {
    if (typeof content === "string") {
      content = encoder.encode(content);
    }
    if (!key) {
      key = this.getSecretKey() || this.getEncryptionKey();
    }

    const alg = {
      name: key.algorithm.name
    };
    if (alg.name.startsWith("AES")) {
      if (!iv) {
        iv = this.iv;
      }
      if (!iv) {
        iv = window.crypto.getRandomValues(new Uint8Array(12));
        this.iv = iv;
      }
      alg.iv = iv;
    }
    return new Uint8Array(await window.crypto.subtle.encrypt(
      alg,
      key,
      content
    ));
  }

  async decrypt(content, key, convertToStr = true, iv = this.iv) {
    const myDecKey = this.getDecryptionKey() || this.getSecretKey();
    if (!myDecKey) {
      throw new Error("You don't have access to that yet");
    }
    if (!key) {
      key = this.getSecretKey() || this.getDecryptionKey();
    }

    if (!content || (myDecKey.algorithm.name.startsWith("AES") && !iv)) {
      throw new Error("both content and iv are required");
    }

    const alg = {
      name: key.algorithm.name
    };
    if (alg.name.startsWith("AES")) {
      alg.iv = iv;
    }

    const decrypted = await window.crypto.subtle.decrypt(
      alg,
      key,
      content
    );

    return convertToStr ? decoder.decode(decrypted) : decrypted;
  }

  async serialize() {
    const obj = {
      iv: this.iv,
      _id: this._id
    };

    if (this.encryptionKey) {
      obj.encryptionKey = JSON.stringify(await crypto.subtle.exportKey("jwk", this.encryptionKey));
      if (this._symmetricKey) {
        obj._symmetricKey = await this.wrapKey(this._symmetricKey);
        obj.iv = this.iv;
      }
    }
    if (this.verificationKey) {
      obj.verificationKey = JSON.stringify(await crypto.subtle.exportKey("jwk", this.verificationKey));
    }

    return obj;
  }
}

Principal.createEncryptingEntity = async function createEncryptingEntity(_id) {
  const princ = new Principal(_id);
  princ.iv = window.crypto.getRandomValues(new Uint8Array(12));
  princ._symmetricKey = await generateSymmetricEncryptionKey();
  return princ;
};

Principal.createAccessingEntity = async function createEncryptingEntity(_id) {
  const princ = new Principal(_id);
  const kp = await generateAssymetricEncryptionKeyPair();
  princ.setEncryptionKey(kp.publicKey);
  princ.setDecryptionKey(kp.privateKey);
  princ._symmetricKey = await generateSymmetricEncryptionKey();
  return princ;
};

Principal.loadAccessingEntity = async function loadAccessingEntity(object) {
  const princ = new Principal(object._id);
  princ.iv = object.iv;
  if (object.encryptionKey) {
    princ._symmetricKey = object._symmetricKey;
    await princ.importEncryptionKey(JSON.parse(object.encryptionKey));
  }
  princ.principalAccess = object.principalAccess;
  /*
  object.principalAccess.forEach((pa) => {
    princ.principalAccess.push({
      type: pa.type,
      principalId: pa.principalId,
      key: pa.key
    });
  });*/

  return princ;
};


export async function getMasterPrincipal(password, _id) {
  const derivedKey = await deriveKeyFromPassword(password, _id);
  const master = new Principal(_id);
  master.setEncryptionKey(derivedKey);
  return master;
}


export class PrincipalProvider {
  constructor() {
    this._principalCache = new Map();
  }

  async init(masterPrincipalId, password) {
    if (masterPrincipalId) {
      const generatedMasterPrincipal = await getMasterPrincipal(password, masterPrincipalId);
      this._userMasterPrincipal = await this.getPrincipal(masterPrincipalId);
      this._userMasterPrincipal._symmetricKey = generatedMasterPrincipal._symmetricKey;
      await this._userMasterPrincipal.init();
      this._userPrincipal = await this.getPrincipal(this._userMasterPrincipal.principalAccess[0].principalId, true, this._userMasterPrincipal);
      await this._userPrincipal.init();
    }
    else {
      this._userMasterPrincipal = await getMasterPrincipal(password, this.getId());
      await this._userMasterPrincipal.init();
      this._userPrincipal = await Principal.createAccessingEntity(this.getId());
      await this._userPrincipal.init();
      const accessRequest = await this._userPrincipal.grantAccessFor(this._userMasterPrincipal);
      await this.storePrincipal(this._userMasterPrincipal);
      await this.storePrincipal(this._userPrincipal);
      await this.saveAccess(this._userMasterPrincipal._id, accessRequest);
      await this.saveMasterPrincipalId(this._userMasterPrincipal._id);
    }
  }

  async newEncryptionPrincipal(grantAccess = true) {
    const princ = await Principal.createEncryptingEntity(this.getId());
    if (grantAccess) {
      const accessRequest = await princ.grantAccessFor(this._userPrincipal);
      await princ.init();
      await this.saveAccess(this._userPrincipal._id, accessRequest);
    }
    return princ;
  }
}
