const Principals = new Meteor.Collection("principals");
const PrincipalAccessRequests = new Meteor.Collection("principalAccessRequests");
const PrincipalAccesses = new Meteor.Collection("principalAccesses");
Meteor.methods({
  storePrincipal(principal) {
    Principals.update({ _id: principal._id }, { $set: principal }, { upsert: true });
  },
  getPrincipal(principalId) {
    const princ = Principals.findOne({ _id: principalId });
    if (princ) {
      princ.principalAccess = PrincipalAccesses.find({ fromPrincipalId: principalId }).fetch();
    }
    return princ;
  },
  storeMasterPrincipalId(masterPrincipalId) {
    if (!Meteor.userId()) {
      throw new Meteor.Error("Not logged in");
    }
    Meteor.users.update({ _id: Meteor.userId() }, { $set: { masterPrincipalId } });
  },
  requestAccess(fromPrincipalId, toPrincipalId) {
    PrincipalAccessRequests.insert({
      fromPrincipalId,
      toPrincipalId
    });
  },
  getAccessRequests(toPrincipalId) {
    return PrincipalAccessRequests.find({ toPrincipalId }).fetch();
  },
  saveAccess(fromPrincipalId, encryptedToPrincipalIdAndKey) {
    PrincipalAccesses.insert({ fromPrincipalId, encryptedToPrincipalIdAndKey });
  }
});
