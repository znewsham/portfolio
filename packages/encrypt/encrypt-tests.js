// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by encrypt.js.
import { name as packageName } from "meteor/znewsham:encrypt";

// Write your tests here!
// Here is an example.
Tinytest.add('encrypt - example', function (test) {
  test.equal(packageName, "encrypt");
});
