import { Random } from "meteor/random";
import { Principal, PrincipalProvider } from "./principal.js";

function promisifyCall(...args) {
  return new Promise((resolve, reject) => {
    Meteor.call(...args, (err, res) => {
      if (err) {
        return reject(err);
      }
      resolve(res);
    });
  });
}

export class MeteorPrincipalProvider extends PrincipalProvider {
  constructor() {
    super();
    this.dep = new Tracker.Dependency();
  }

  ready() {
    this.dep.depend();
    return !!this._userPrincipal;
  }

  async init(...args) {
    await super.init(...args);
    this.dep.changed();
  }

  getId() {
    return Random.id();
  }

  async saveMasterPrincipalId(masterPrincipalId) {
    return promisifyCall("storeMasterPrincipalId", masterPrincipalId);
  }

  async getAccessRequests(toPrincipalId) {
    return promisifyCall("getAccessRequests", toPrincipalId);
  }

  async requestAccess(fromPrincipalId, toPrincipalId) {
    return promisifyCall("requestAccess", fromPrincipalId, toPrincipalId);
  }

  async saveAccess(fromPrincipalId, encryptedToPrincipalIdAndKey) {
    return promisifyCall("saveAccess", fromPrincipalId, encryptedToPrincipalIdAndKey);
  }

  async getPrincipal(principalId, refresh = false, unlockWith = null) {
    if (this._principalCache.has(principalId) && !refresh) {
      return this._principalCache.get(principalId);
    }
    const fromPrincipalObject = await promisifyCall("getPrincipal", principalId);
    if (!fromPrincipalObject) {
      return null;
    }
    const fromPrincipal = await Principal.loadAccessingEntity(fromPrincipalObject);
    if (unlockWith && fromPrincipal) {
      await fromPrincipal.unlockWith(unlockWith);
    }
    if (fromPrincipal) {
      this._principalCache.set(principalId, fromPrincipal);
    }
    return fromPrincipal;
  }

  async getPath(fromPrincipal, toPrincipal) {
    if (!fromPrincipal) {
      throw new Meteor.Error("Must provide a from principal");
    }
    if (!toPrincipal) {
      throw new Meteor.Error("Must provide a to principal");
    }

    if (!toPrincipal._id) {
      toPrincipal = await this.getPrincipal(toPrincipal);
    }
    if (!toPrincipal) {
      return null;
    }
    if (toPrincipal._id === fromPrincipal._id) {
      return [fromPrincipal];
    }
    if (!fromPrincipal._id) {
      fromPrincipal = await this.getPrincipal(fromPrincipal, false, this._userPrincipal);
    }
    if (!fromPrincipal || !fromPrincipal.isUnlocked()) {
      throw new Meteor.Error("You don't have access to that principal");
    }
    await fromPrincipal.init();
    if (await fromPrincipal.hasAccessTo(toPrincipal)) {
      if (!toPrincipal.isUnlocked()) {
        await toPrincipal.unlockWith(fromPrincipal);
      }
      return [fromPrincipal, toPrincipal];
    }
    return null;
  }

  async storePrincipal(principal) {
    if (!principal.isUnlocked()) {
      throw new Meteor.Error("You can't save a principal you have no access to");
    }

    return promisifyCall("storePrincipal", await principal.serialize());
  }
}
