Package.describe({
  name: 'templating',
  version: '1.3.2',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.8');
  api.use('ecmascript');
  api.use("blaze@2.3.0");
  api.mainModule('templating.js', ["client", "server"], { lazy: true });
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('templating');
  api.mainModule('templating-tests.js');
});
