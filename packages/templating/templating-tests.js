// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by templating.js.
import { name as packageName } from "meteor/templating";

// Write your tests here!
// Here is an example.
Tinytest.add('templating - example', function (test) {
  test.equal(packageName, "templating");
});
