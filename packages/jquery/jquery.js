// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See jquery-tests.js for an example of importing.
window.$ = require("jquery");
window.jQuery = window.$;
export const $ = window.$;
export const jQuery = window.jQuery;
