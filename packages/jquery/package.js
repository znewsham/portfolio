Package.describe({
  name: 'jquery',
  version: '1.11.11',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

// NOTE: intentionally NOT npm depending this - if we do, meteor will bundle jquery in here, we don't want it to - we want jquery to be bundled with the rest of the npm modules.
/*
Npm.depends({
  "jquery": "3.4.1"
});
*/
Package.onUse(function(api) {
  api.versionsFrom('1.0');
  api.use('ecmascript');

  // NOTE: jquery can't be lazy :( at a bare minimum cultofcoders:persistent-session requires amplify which assumes jquery
  api.export('$', 'client');
  api.export('jQuery', 'client');
  api.mainModule('jquery.js', "client");
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('jquery');
  api.mainModule('jquery-tests.js');
});
