import "./tracker.js";
import _ from "underscore";
import { ReactiveDict } from "meteor/reactive-dict";


export function markAsHelper(target, property, descriptor) {

}

export class BlazeComponent {
  constructor(templateInstance, settings = {}) {
    this.templateInstance = templateInstance;
    this.settings = new ReactiveDict(settings);
    this.timeouts = [];
    this.intervals = [];
    this.listeners = [];
    if (this.init) {
      this.init();
    }
  }

  nonReactiveData() {
    return this.templateInstance.data;
  }

  reactiveData(...fields) {
    if (fields.length) {
      return Template.partialData(...fields);
    }
    return Template.currentData();
  }

  once(...args) {
    if (!Tracker.once) {
      throw new Meteor.Error("You don't have Tracker.once");
    }
    return Tracker.once.call(this, ...args);
  }

  autorun(...args) {
    return this.templateInstance.autorun(...args);
  }

  subscribe(...args) {
    return this.templateInstance.subscribe(...args);
  }

  _statChanged(fields = [], callback = () => {}, strict = false) {
    return this.autorun((computation) => {
      let state;
      if (fields.length === 0) {
        state = this.settings.all();
      }
      else {
        state = _.object(fields, fields.map(f => this.get(f)));
      }
      if (!strict) {
        callback.call(this, state, computation);
      }
      else {
        Tracker.nonreactive(() => callback.call(this, state, computation));
      }
    });
  }

  stateChanged(...fields) {
    const callback = fields.splice(fields.length - 1, 1)[0];
    return this._statChanged(fields, callback, false);
  }

  statChangedStrict(...fields) {
    const callback = fields.splice(fields.length - 1, 1)[0];
    return this._statChanged(fields, callback, true);
  }

  _dataChanged(fields = [], callback = () => {}, strict = false) {
    return this.autorun((computation) => {
      const data = fields.length === 0 ? Template.currentData() : Template.partialData(...fields);
      if (!strict) {
        callback.call(this, data, computation);
      }
      else {
        Tracker.nonreactive(() => callback.call(this, data, computation));
      }
    });
  }

  dataChanged(...fields) {
    const callback = fields.splice(fields.length - 1, 1)[0];
    return this._dataChanged(fields, callback, false);
  }

  dataChangedStrict(...fields) {
    const callback = fields.splice(fields.length - 1, 1)[0];
    return this._dataChanged(fields, callback, true);
  }

  set(settingName, value) {
    this.settings.set(settingName, value);
  }

  get(settingName) {
    return this.settings.get(settingName);
  }

  setTimeout(callback, time, { name } = {}) {
    if (name) {
      const oldEntry = this.timeouts.find(i => i.name === name);
      if (oldEntry) {
        this.clearInterval(oldEntry.id);
        this.timeouts.splice(this.timeouts.indexOf(oldEntry), 1);
      }
    }
    const id = Meteor.setTimeout(callback, time);
    this.timeouts.push({
      id,
      name
    });
    return id;
  }

  clearTimeout(id) {
    Meteor.clearTimeout(id);
    this.timeouts.splice(this.timeouts.indexOf(id), 1);
  }

  setInterval(callback, interval, { name } = {}) {
    if (name) {
      const oldEntry = this.intervals.find(i => i.name === name);
      if (oldEntry) {
        this.clearInterval(oldEntry.id);
        this.intervals.splice(this.intervals.indexOf(oldEntry), 1);
      }
    }
    const id = Meteor.setInterval(callback, interval);
    this.intervals.push({
      id,
      name
    });
    return id;
  }

  clearInterval(id) {
    Meteor.clearInterval(id);
    this.intervals.splice(this.intervals.indexOf(id), 1);
  }

  $(...args) {
    return this.templateInstance.$(...args);
  }

  on(elementOrSelector, event, f) {
    this.listeners.push({
      event,
      elementOrSelector,
      f
    });
    $(elementOrSelector).on(event, f);
  }


  destructor() {
    this.intervals.forEach(entry => Meteor.clearInterval(entry.id));
    this.timeouts.forEach(entry => Meteor.clearTimeout(entry.id));
    this.listeners.forEach(entry => $(entry.elementOrSelector).off(entry.event, entry.f));
  }
}

BlazeComponent.register = function register(template, Klass) {
  const events = Klass.EventMap ? Klass.EventMap() : {};
  template.events(_.object(_.keys(events), _.map(events, (handlerName) => {
    return function eventHandler(event, templateInstance, ...extra) {
      const componentInstance = templateInstance._component;
      componentInstance[handlerName].call(componentInstance, event, templateInstance, ...extra, this);
    };
  })));

  const helpers = Klass.HelperMap ? Klass.HelperMap() : {};
  if (_.isArray(helpers)) {
    template.helpers(_.object(helpers, helpers.map(handlerName => {
      return function helper(...args) {
        const componentInstance = Template.instance()._component;
        return componentInstance[handlerName].call(componentInstance, ...args);
      };
    })));
  }
  else {
    template.helpers(_.object(_.keys(helpers), _.map(helpers, (handlerName) => {
      return function helper(...args) {
        const componentInstance = Template.instance()._component;
        return componentInstance[handlerName].call(componentInstance, ...args);
      };
    })));
  }

  template.onCreated(function onCreated() {
    if (Klass.Schema) {
      const schema = Klass.Schema();
      const context = schema.namedContext("myContext");
      const isValid = context.validate(this.data);
      if (!isValid) {
        const error = new Meteor.Error(500, `Invalid data\n${context.invalidKeys().join("\n")}`);
        if (process.env.NODE_ENV === "development") {
          throw error;
        }
        else {
          console.error(error);
        }
      }
    }
    this._component = new Klass(this);
  });

  if (Klass.prototype.destructor) {
    template.onDestroyed(function onDestroyed() {
      this._component.destructor();
      delete this._component;
    });
  }

  if (Klass.prototype.rendered) {
    template.onRendered(function onRendered() {
      this._component.rendered();
    });
  }
};
