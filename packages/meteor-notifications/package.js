Package.describe({
  name: "gfk:notifications",
  summary: "Notifications - Add reactive notifications to any meteor template",
  version: "1.1.4",
  git: "https://github.com/gfk-ba/meteor-notifications"
});

Package.onUse((api) => {
  api.versionsFrom("METEOR@1.8");

  api.use([
    "ecmascript",
    "templating",
    "blaze",
    "underscore",
    "less",
    "mongo"
  ], "client");
  api.mainModule("index.js", "client", { lazy: true });
});

Package.onTest((api) => {
  api.use([
    "tinytest",
    "underscore",
    "gfk:notifications"
  ], "client");

  api.use(["spacejamio:munit@2.1.0"], "client");

  api.addFiles("notifications_tests.js", "client");
});
