// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by blaze-html-templates.js.
import { name as packageName } from "meteor/blaze-html-templates";

// Write your tests here!
// Here is an example.
Tinytest.add('blaze-html-templates - example', function (test) {
  test.equal(packageName, "blaze-html-templates");
});
