Package.describe({
    name: 'teamon:tinymce',
    version: '4.5.4',
    summary: 'TinyMCE editor',
    git: 'https://github.com/teamOnHQ/tinymce.git',
});

Package.onUse(function(api) {
  api.versionsFrom('1.6.0.1');

  api.use('ecmascript');
  api.mainModule('client.js', "client", { lazy: true });
  // client files

  api.addAssets([
      // skins
      'skins/lightgray/content.inline.min.css',
      'skins/lightgray/content.min.css', // modified
      'skins/lightgray/skin.ie7.min.css',
      'skins/lightgray/skin.min.css',

      'skins/lightgray/fonts/tinymce-small.eot',
      'skins/lightgray/fonts/tinymce-small.svg',
      'skins/lightgray/fonts/tinymce-small.ttf',
      'skins/lightgray/fonts/tinymce-small.woff',
      'skins/lightgray/fonts/tinymce.eot',
      'skins/lightgray/fonts/tinymce.svg',
      'skins/lightgray/fonts/tinymce.ttf',
      'skins/lightgray/fonts/tinymce.woff',

      'skins/lightgray/img/anchor.gif',
      'skins/lightgray/img/loader.gif',
      'skins/lightgray/img/object.gif',
      'skins/lightgray/img/trans.gif',
  ], 'client');
});
