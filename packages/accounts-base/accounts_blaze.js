
if (Package.blaze) {
  const { Template } = Package.blaze.Blaze;

  /**
   * @global
   * @name  currentUser
   * @isHelper true
   * @summary Calls [Meteor.user()](#meteor_user). Use `{{#if currentUser}}` to check whether the user is logged in.
   */
  Template.registerHelper("currentUser", () => Meteor.user());

  /**
   * @global
   * @name  loggingIn
   * @isHelper true
   * @summary Calls [Meteor.loggingIn()](#meteor_loggingin).
   */
  Template.registerHelper("loggingIn", () => Meteor.loggingIn());

  /**
   * @global
   * @name  loggingOut
   * @isHelper true
   * @summary Calls [Meteor.loggingOut()](#meteor_loggingout).
   */
  Template.registerHelper("loggingOut", () => Meteor.loggingOut());

  /**
   * @global
   * @name  loggingInOrOut
   * @isHelper true
   * @summary Calls [Meteor.loggingIn()](#meteor_loggingin) or [Meteor.loggingOut()](#meteor_loggingout).
   */
  Template.registerHelper(
    "loggingInOrOut",
    () => Meteor.loggingIn() || Meteor.loggingOut()
  );
}
