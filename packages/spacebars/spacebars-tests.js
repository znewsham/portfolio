// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by spacebars.js.
import { name as packageName } from "meteor/spacebars";

// Write your tests here!
// Here is an example.
Tinytest.add('spacebars - example', function (test) {
  test.equal(packageName, "spacebars");
});
