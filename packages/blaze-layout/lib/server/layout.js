import "meteor/blaze";

export const BlazeLayout = {};

BlazeLayout.readyDep = new Tracker.Dependency();
BlazeLayout.initialRenderComplete = function initialRenderComplete(templateInstance) {
  if (templateInstance && templateInstance.view && templateInstance.view) {
    let root = templateInstance.view;
    while (root && root.parentView) {
      root = root.parentView;
    }
    root._isReady = true;
    BlazeLayout.readyDep.changed();
  }
};
BlazeLayout.render = function render(layout, regions) {
  regions = regions || {};
  const element = document.createElement("div");
  const view = Blaze.renderWithData(
    Template[layout],
    regions,
    element
  );
  return {
    element,
    view
  };
};
