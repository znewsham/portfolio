Package.describe({
  summary: "Layout Manager for Blaze (works well with FlowRouter)",
  version: "2.3.0",
  git: "https://github.com/kadirahq/blaze-layout",
  name: "kadira:blaze-layout"
});

Package.onUse((api) => {
  configure(api);
});

Package.onTest((api) => {
  configure(api);
  api.use("tinytest");
  api.addFiles("tests/client/init.templates.html", "client");
  api.addFiles("tests/client/init.templates.js", "client");
  api.addFiles("tests/client/unit.js", "client");
  api.addFiles("tests/client/integration.js", "client");
});

function configure(api) {
  api.versionsFrom("1.8");
  api.use("ecmascript");
  api.use("blaze");
  api.use("templating");
  api.use("reactive-dict");
  api.use("underscore");
  api.use("jquery");
  api.use("tracker");

  api.mainModule("lib/client/layout.js", "client", { lazy: true });
  api.mainModule("lib/server/layout.js", "server", { lazy: true });
}
